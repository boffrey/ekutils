// 
// EkUtils 
// 

package ekdao.dto;

import servconn.dto.usercard.UserCard;
import servconn.dto.card.Card;
import java.util.List;

public class UnifiedCard
{
    private String cardId;
    private String cardName;
    private List<String> skills;
    private String evoSkill;
    private Integer cost;
    private String faction;
    private List<Integer> hpArray;
    private List<Integer> atkArray;
    private Integer level;
    private Integer evo;
    private Integer waitTime;
    private Card card;
    private UserCard userCard;
    
    public Integer getWaitTime() {
        return this.waitTime;
    }
    
    public void setWaitTime(final Integer waitTime) {
        this.waitTime = waitTime;
    }
    
    public String getCardId() {
        return this.cardId;
    }
    
    public void setCardId(final String cardId) {
        this.cardId = cardId;
    }
    
    public String getCardName() {
        return this.cardName;
    }
    
    public void setCardName(final String cardName) {
        this.cardName = cardName;
    }
    
    public List<String> getSkills() {
        return this.skills;
    }
    
    public void setSkills(final List<String> skills) {
        this.skills = skills;
    }
    
    public Integer getCost() {
        return this.cost;
    }
    
    public void setCost(final Integer cost) {
        this.cost = cost;
    }
    
    public String getFaction() {
        return this.faction;
    }
    
    public void setFaction(final String faction) {
        this.faction = faction;
    }
    
    public List<Integer> getHpArray() {
        return this.hpArray;
    }
    
    public void setHpArray(final List<Integer> hpArray) {
        this.hpArray = hpArray;
    }
    
    public List<Integer> getAtkArray() {
        return this.atkArray;
    }
    
    public void setAtkArray(final List<Integer> atkArray) {
        this.atkArray = atkArray;
    }
    
    public Integer getLevel() {
        return this.level;
    }
    
    public void setLevel(final Integer level) {
        this.level = level;
    }
    
    public Integer getEvo() {
        return this.evo;
    }
    
    public void setEvo(final Integer evo) {
        this.evo = evo;
    }
    
    public String getEvoSkill() {
        return this.evoSkill;
    }
    
    public void setEvoSkill(final String evoSkill) {
        this.evoSkill = evoSkill;
    }
    
    public Card getCard() {
        return this.card;
    }
    
    public void setCard(final Card card) {
        this.card = card;
    }
    
    public void setUserCard(final UserCard userCard) {
        this.userCard = userCard;
    }
    
    public UserCard getUserCard() {
        return this.userCard;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("UnifiedCard [cardId=");
        builder.append(this.cardId);
        builder.append(", cardName=");
        builder.append(this.cardName);
        builder.append(", skills=");
        builder.append(this.skills);
        builder.append(", evoSkill=");
        builder.append(this.evoSkill);
        builder.append(", cost=");
        builder.append(this.cost);
        builder.append(", faction=");
        builder.append(this.faction);
        builder.append(", hpArray=");
        builder.append(this.hpArray);
        builder.append(", atkArray=");
        builder.append(this.atkArray);
        builder.append(", level=");
        builder.append(this.level);
        builder.append(", evo=");
        builder.append(this.evo);
        builder.append(", waitTime=");
        builder.append(this.waitTime);
        builder.append(", card=");
        builder.append(this.card);
        builder.append(", userCard=");
        builder.append(this.userCard);
        builder.append("]");
        return builder.toString();
    }
}

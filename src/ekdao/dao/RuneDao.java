// 
// EkUtils 
// 

package ekdao.dao;

import servconn.dto.rune.Rune;

public interface RuneDao
{
    Rune getRune(final String p0);
    
    Rune getRuneById(final String p0);
    
    Rune getRuneByName(final String p0);
}

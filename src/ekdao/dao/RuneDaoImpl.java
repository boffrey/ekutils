package ekdao.dao;

import org.apache.logging.log4j.LogManager;
import com.google.gson.GsonBuilder;
import java.util.List;
import java.io.FileNotFoundException;
import java.io.FileReader;
import servconn.dto.rune.RuneData;
import java.io.File;
import java.util.HashMap;
import org.apache.logging.log4j.Logger;
import servconn.dto.rune.Rune;
import java.util.Map;
import com.google.gson.Gson;

public class RuneDaoImpl implements RuneDao
{
    private static Gson GSON;
    private Map<String, Rune> runeIdMap;
    private Map<String, Rune> runeNameMap;
    private static final Logger logger;
    
    public RuneDaoImpl(final File file) {
        this.runeIdMap = new HashMap<String, Rune>();
        this.runeNameMap = new HashMap<String, Rune>();
        try {
            final RuneData runeData = RuneDaoImpl.GSON.fromJson(new FileReader(file), RuneData.class);
            final List<Rune> runeList = runeData.getData().getRunes();
            for (final Rune rune : runeList) {
                this.runeIdMap.putIfAbsent(rune.getRuneId(), rune);
                this.runeNameMap.putIfAbsent(rune.getRuneName().toLowerCase(), rune);
            }
        }
        catch (FileNotFoundException e) {
            RuneDaoImpl.logger.fatal(e);
        }
    }
    
    @Override
    public Rune getRuneById(final String runeId) {
        return this.runeIdMap.get(runeId);
    }
    
    @Override
    public Rune getRuneByName(final String runeName) {
        return this.runeNameMap.get(runeName.toLowerCase());
    }
    
    @Override
    public Rune getRune(final String runeKey) {
        Rune rune = this.getRuneById(runeKey);
        if (rune == null) {
            rune = this.getRuneByName(runeKey);
        }
        return rune;
    }
    
    static {
        RuneDaoImpl.GSON = new GsonBuilder().setPrettyPrinting().create();
        logger = LogManager.getLogger();
    }
}

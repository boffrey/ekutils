// 
// EkUtils 
// 

package ekdao.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import servconn.dto.card.Card;
import servconn.dto.card.CardData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ekdao.util.DaoConstants;

public class CardDaoImpl implements CardDao
{
    private static Gson GSON;
    private HashMap<String, Card> cardIdMap;
    private HashMap<String, Card> cardNameMap;
    private static final Logger logger;
    
    public CardDaoImpl(final File file) {
        this.cardIdMap = new HashMap<String, Card>();
        this.cardNameMap = new HashMap<String, Card>();
        try {
            CardDaoImpl.logger.debug("Card file " + file.getAbsolutePath());
            final CardData cardData = CardDaoImpl.GSON.fromJson(new FileReader(file), CardData.class);
            final List<Card> cardList = cardData.getData().getCards();
            for (final Card card : cardList) {
                this.cardIdMap.putIfAbsent(card.getCardId(), card);
                this.cardNameMap.putIfAbsent(card.getCardName().toLowerCase(), card);
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public Card getCardById(final String cardId) {
        return this.cardIdMap.get(cardId);
    }
    
    @Override
    public Card getCardByName(final String cardName) {
        return this.cardNameMap.get(cardName.toLowerCase());
    }
    
    @Override
    public Card getCard(final String cardKey) {
        Card card = this.getCardById(cardKey);
        if (card == null) {
            card = this.getCardByName(cardKey);
        }
        return card;
    }
    
    @Override
    public Integer getCardHpByLevel(final String cardKey, final Integer level) {
        final Card card = this.getCard(cardKey);
        if (card == null) {
            return 0;
        }
        return card.getHpArray().get(level);
    }
    
    @Override
    public Integer getCardAttackByLevel(final String cardKey, final Integer level) {
        final Card card = this.getCard(cardKey);
        if (card == null) {
            return 0;
        }
        return card.getAttackArray().get(level);
    }
    
    @Override
    public List<String> getFactionCardsByStar(final String star) {
        final List<String> cardList = new ArrayList<String>();
        if (StringUtils.isEmpty(star)) {
            return cardList;
        }
        this.cardIdMap.values().stream().filter(e -> star.equals(e.getColor())).filter(e -> DaoConstants.FACTIONS.contains(e.getRace())).forEach(e -> cardList.add(e.getCardId()));
        return cardList;
    }
    
    private List<String> filterCardsByRace(final String race) {
        final List<String> cardList = new ArrayList<String>();
        if (StringUtils.isEmpty(race)) {
            return cardList;
        }
        this.cardIdMap.values().stream().filter(e -> race.equals(e.getRace())).forEach(e -> cardList.add(e.getCardId()));
        return cardList;
    }
    
    @Override
    public List<String> getSpecialCards() {
        return this.filterCardsByRace("99");
    }
    
    @Override
    public List<String> getGoldCards() {
        return this.filterCardsByRace("95");
    }
    
    @Override
    public List<String> getExperienceCards() {
        return this.filterCardsByRace("96");
    }
    
    @Override
    public List<String> getDemons() {
        return this.filterCardsByRace("100");
    }
    
    @Override
    public List<String> getHydras() {
        final List<String> cardList = new ArrayList<String>();
        for (final Card card : this.cardIdMap.values()) {
            if ("97".equals(card.getRace()) && !card.getCardName().contains("Legendary")) {
                cardList.add(card.getCardId());
            }
        }
        return cardList;
    }
    
    @Override
    public List<String> getEWBosses() {
        final List<String> cardList = new ArrayList<String>();
        for (final Card card : this.cardIdMap.values()) {
            if ("97".equals(card.getRace()) && card.getCardName().contains("Legendary")) {
                cardList.add(card.getCardId());
            }
        }
        return cardList;
    }
    
    static {
        CardDaoImpl.GSON = new GsonBuilder().setPrettyPrinting().create();
        logger = LogManager.getLogger();
    }
}

package ekdao;

import servconn.dto.rune.Rune;
import servconn.dto.skill.Skill;
import java.util.List;
import servconn.dto.card.Card;
import ekdao.dao.RuneDao;
import ekdao.dao.SkillDao;
import ekdao.dao.CardDao;
import ekdao.dao.DaoFactory;

public class App
{
    public static void main(final String[] args) {
        try {
            final CardDao cardDao = DaoFactory.getCardDao("cards.json");
            final SkillDao skillDao = DaoFactory.getSkillDao("skills.json");
            final RuneDao runeDao = DaoFactory.getRuneDao("runes.json");
            final Card card = cardDao.getCard("the don");
            final List<String> skillsDon = skillDao.getCardSkills(card);
            for (final String skill : skillsDon) {
                System.out.println(skill);
            }
            System.out.println(cardDao.getCardById("68"));
            final Card card2 = cardDao.getCard("68");
            final List<String> skills = skillDao.getCardSkills(card2);
            for (final String skill2 : skills) {
                System.out.println(skill2);
            }
            final Skill skill3 = skillDao.getSkill("Trap 2");
            System.out.println(skill3);
            final Rune rune = runeDao.getRune("Mineral");
            System.out.println(rune);
        }
        catch (IllegalArgumentException iae) {
            System.out.println(iae.getMessage());
        }
    }
}

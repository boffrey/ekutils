// 
// EkUtils 
// 

package ekdao.util;

public class Util
{
    public static <T extends Enum<T>> T getEnumFromString(final Class<T> c, final String string) {
        if (c != null && string != null) {
            try {
                return Enum.valueOf(c, string.trim().toUpperCase());
            }
            catch (IllegalArgumentException ex) {}
        }
        return null;
    }
}

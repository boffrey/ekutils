package ekdao.util;

import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;

public class DaoConstants
{
    public static final String NL;
    public static final String[] FACTION_VALUES;
    public static final Set<String> FACTIONS;
    public static final String SPECIAL_CARD = "99";
    public static final String DEMON_CARD = "100";
    public static final String HYDRA_CARD = "97";
    public static final String LEGENDARY_CARD = "97";
    public static final String GOLD_CARD = "95";
    public static final String FEAST_CARD = "96";
    
    static {
        NL = System.getProperty("line.separator");
        FACTION_VALUES = new String[] { "1", "2", "3", "4" };
        FACTIONS = new HashSet<String>(Arrays.asList(DaoConstants.FACTION_VALUES));
    }
}

package servconn.client;

public enum Server
{
    equilibrium("s1.ek"), 
    aeon("s2.ek"), 
    ethereal("s3.ek"), 
    ascension("s4.ek"), 
    skorn("s1.ekru"), 
    apollo("s1.ekbb");
    
    private final String strValue;
    
    private Server(final String strValue) {
        this.strValue = strValue;
    }
    
    @Override
    public String toString() {
        return this.strValue;
    }
}

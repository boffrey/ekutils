// 
// EkUtils 
// 

package servconn.client;

import servconn.dto.user.UserInfo;
import servconn.dto.login.ServerLoginDto;
import servconn.dto.login.Player;

public class LoginData
{
    private boolean loggedIn;
    private Player player;
    private ServerLoginDto serverLogin;
    private String server;
    private String serverUrl;
    private String email;
    private String password;
    private UserInfo userInfo;
    
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(final String password) {
        this.password = password;
    }
    
    public String getServer() {
        return this.server;
    }
    
    public void setServer(final String server) {
        this.server = server;
    }
    
    public String getServerUrl() {
        return this.serverUrl;
    }
    
    public void setServerUrl(final String serverUrl) {
        this.serverUrl = serverUrl;
    }
    
    public ServerLoginDto getServerLogin() {
        return this.serverLogin;
    }
    
    public void setServerLogin(final ServerLoginDto serverLogin) {
        this.serverLogin = serverLogin;
    }
    
    public LoginData() {
        this.loggedIn = false;
    }
    
    public boolean isLoggedIn() {
        return this.loggedIn;
    }
    
    public void setLoggedIn(final boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
    
    public Player getPlayer() {
        return this.player;
    }
    
    public void setPlayer(final Player player) {
        this.player = player;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("LoginData [loggedIn=");
        builder.append(this.loggedIn);
        builder.append(", player=");
        builder.append(this.player);
        builder.append(", serverLogin=");
        builder.append(this.serverLogin);
        builder.append(", server=");
        builder.append(this.server);
        builder.append(", serverUrl=");
        builder.append(this.serverUrl);
        builder.append(", email=");
        builder.append(this.email);
        builder.append(", password=");
        builder.append(this.password);
        builder.append("]");
        return builder.toString();
    }
    
    public void setUserInfo(final UserInfo userInfo) {
        this.userInfo = userInfo;
    }
    
    public UserInfo getUserInfo() {
        return this.userInfo;
    }
}

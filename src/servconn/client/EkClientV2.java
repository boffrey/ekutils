package servconn.client;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import servconn.dto.card.Card;
import servconn.dto.card.CardData;
import servconn.dto.league.LeagueData;
import servconn.dto.login.GuestLoginDto;
import servconn.dto.login.Player;
import servconn.dto.login.ServerLoginDto;
import servconn.dto.login.ServerLoginUserInfo;
import servconn.dto.rune.Rune;
import servconn.dto.rune.RuneData;
import servconn.dto.skill.Skill;
import servconn.dto.skill.SkillData;
import servconn.util.Constants;

public class EkClientV2
{
    private OkHttpClient client;
    private CookieManager cookieManager;
    private static Gson GSON;
    private Map<String, Card> cardMap;
    private Map<String, Skill> skillMap;
    private Map<String, Rune> runeMap;
    private boolean isLogin;
    
    public EkClientV2() {
        this.client = new OkHttpClient();
        (this.cookieManager = new CookieManager()).setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        this.client.setCookieHandler(this.cookieManager);
        this.isLogin = false;
    }
    
    private Response doGetRequest(final String url) throws IOException {
        final Request request = new Request.Builder().url(url).build();
        final Response response = this.client.newCall(request).execute();
        return response;
    }
    
    private Response doPostRequest(final String url, final RequestBody formBody) throws IOException {
        final Request request = new Request.Builder().url(url).post(formBody).build();
        final Response response = this.client.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }
        return response;
    }
    
    private Player getConnectionData() throws IOException {
        final Response response = this.doGetRequest("http://mobile.arcgames.com//user/playasguest?gameid=51");
        final String json = response.body().string();
        final Player p = this.getPlayerFromConnectionData(json);
        return p;
    }
    
    public String loginWithUser(final String email, final String password) throws IOException {
        final Response response = this.doGetRequest("http://mobile.arcgames.com/user/login?gameid=51");
        final String body = response.body().string();
        System.out.println(body);
        final Document doc = Jsoup.parse(body);
        final Element userEl = doc.getElementById("un");
        final String nameOfEmailField = userEl.attr("name");
        final Element passEl = doc.getElementById("pw");
        final String nameOfPasswordField = passEl.attr("name");
        final Element captchaEl = doc.getElementById("captcha_login");
        final String nameOfCaptchaField = captchaEl.attr("name");
        System.out.println(nameOfEmailField);
        System.out.println(nameOfPasswordField);
        System.out.println(nameOfCaptchaField);
        final RequestBody formBody = new FormEncodingBuilder().add(nameOfEmailField, email).add(nameOfPasswordField, password).add(nameOfCaptchaField, "").build();
        final Response r = this.doPostRequest("http://mobile.arcgames.com/user/login?gameid=51", formBody);
        System.out.println("-------------------");
        final String json = r.body().string();
        return json;
    }
    
    public String getLoginPage() throws IOException {
        final String json = this.loginWithUser("", "");
        final Player p = this.getPlayerFromConnectionData(json);
        final ServerLoginDto serverLogin = this.getServerLoginInfo(p);
        this.doMpLogin(p, serverLogin, "http://s4.ek.ifreeteam.com/");
        this.getTopEventRanks(p);
        return "";
    }
    
    public void getTopEventRanks(final Player px) throws IOException {
        System.out.println("-------------------");
        final String url = "http://s4.ek.ifreeteam.com/user.php?do=RankTopActivtiyGmt&type=WorldTree";
        final RequestBody formBody = new FormEncodingBuilder().add("Type", "WorldTree").build();
        final Response r = this.doPostRequest("http://s4.ek.ifreeteam.com/user.php?do=RankTopActivtiyGmt&phpl=EN", formBody);
        System.out.println(r.body().string());
    }
    
    private ServerLoginDto getServerLoginInfo(final Player p) throws IOException {
        final RequestBody formBody = new FormEncodingBuilder().add("plat", "pwe").add("uin", p.getUin()).add("nickName", p.getUin()).add("Devicetoken", p.getDeviceToken()).add("userType", "2").build();
        ServerLoginDto serverLogin = new ServerLoginDto();
        final Response r = this.doPostRequest("http://master.ek.ifreeteam.com/mpassport.php?do=plogin", formBody);
        serverLogin = EkClientV2.GSON.fromJson(r.body().string(), ServerLoginDto.class);
        return serverLogin;
    }
    
    private void doMpLogin(final Player p, final ServerLoginDto serverLogin, final String serverUrl) throws IOException {
        final ServerLoginUserInfo userInfo = serverLogin.getData().getUinfo();
        final RequestBody formBody = new FormEncodingBuilder().add("plat", "pwe").add("uin", p.getUin()).add("nickName", p.getUin()).add("Devicetoken", p.getDeviceId()).add("userType", "2").add("MUid", userInfo.getMUid().toString()).add("ppsign", userInfo.getPpsign()).add("sign", userInfo.getSign()).add("nick", userInfo.getNick()).add("time", userInfo.getTime().toString()).add("Udid", "00-C0-7B-EF-00-F5").add("Origin", "IOS_PW").build();
        final Response r = this.doPostRequest(serverUrl + "/login.php?do=mpLogin", formBody);
    }
    
    private void login(final String serverUrl) throws IOException {
        final Player p = this.getConnectionData();
        final ServerLoginDto serverLogin = this.getServerLoginInfo(p);
        this.doMpLogin(p, serverLogin, serverUrl);
        this.isLogin = true;
    }
    
    public LeagueData getLeagueData(final String server) throws IOException {
        final String serverUrl = this.getServerUrl(server);
        if (!this.isLogin) {
            this.login(serverUrl);
        }
        final Response r = this.doGetRequest(serverUrl + "/league.php?do=getLeagueInfo&phpl=EN");
        final String json = r.body().string();
        System.out.println(json);
        final LeagueData leagueData = EkClientV2.GSON.fromJson(json, LeagueData.class);
        return leagueData;
    }
    
    public Map<String, Card> getServerCards(final String server) throws IOException {
        final String serverUrl = this.getServerUrl(server);
        if (!this.isLogin) {
            this.login(serverUrl);
        }
        final Response r = this.doGetRequest(serverUrl + "/card.php?do=GetAllCard&phpl=EN");
        final String json = r.body().string();
        final CardData cardData = EkClientV2.GSON.fromJson(json, CardData.class);
        final List<Card> cardList = cardData.getData().getCards();
        this.cardMap = new HashMap<String, Card>();
        for (final Card card : cardList) {
            this.cardMap.put(card.getCardId(), card);
        }
        return this.cardMap;
    }
    
    public Map<String, Skill> getServerSkills(final String server) throws IOException {
        final String serverUrl = this.getServerUrl(server);
        if (!this.isLogin) {
            this.login(serverUrl);
        }
        final Response r = this.doGetRequest(serverUrl + "/card.php?do=GetAllSkill&phpl=EN");
        final String json = r.body().string();
        final SkillData skillData = EkClientV2.GSON.fromJson(json, SkillData.class);
        final List<Skill> skillList = skillData.getData().getSkills();
        this.skillMap = new HashMap<String, Skill>();
        for (final Skill skill : skillList) {
            this.skillMap.put(skill.getSkillId().toString(), skill);
        }
        return this.skillMap;
    }
    
    public Map<String, Rune> getServerRunes(final String server) throws IOException {
        final String serverUrl = this.getServerUrl(server);
        if (!this.isLogin) {
            this.login(serverUrl);
        }
        this.runeMap = new HashMap<String, Rune>();
        final Response r = this.doGetRequest(serverUrl + "/rune.php?do=GetAllRune&phpl=EN");
        final String json = r.body().string();
        final RuneData runeData = EkClientV2.GSON.fromJson(json, RuneData.class);
        final List<Rune> runeList = runeData.getData().getRunes();
        for (final Rune rune : runeList) {
            this.runeMap.put(rune.getRuneId(), rune);
        }
        return this.runeMap;
    }
    
    private String getServerUrl(final String server) {
        final String serverUrl = Constants.SERVERLIST.get(server);
        if (serverUrl == null) {
            throw new IllegalArgumentException("Unknown server error");
        }
        return serverUrl;
    }
    
    private Player getPlayerFromConnectionData(final String json) {
        final GuestLoginDto guestLogin = EkClientV2.GSON.fromJson(json, GuestLoginDto.class);
        final String[] loginStatus = guestLogin.getLoginstatus().split(":");
        final Player p = new Player();
        p.setUin(loginStatus[1]);
        p.setDeviceToken(loginStatus[3]);
        return p;
    }
    
    static {
        EkClientV2.GSON = new GsonBuilder().setPrettyPrinting().create();
    }
}

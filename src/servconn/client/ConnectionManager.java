package servconn.client;

import java.net.CookieManager;
import java.net.CookiePolicy;

import com.squareup.okhttp.OkHttpClient;

public class ConnectionManager
{
    private OkHttpClient client;
    private CookieManager cookieManager;
    private static final ConnectionManager instance;
    
    private ConnectionManager() {
        this.client = new OkHttpClient();
        (this.cookieManager = new CookieManager()).setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        this.client.setCookieHandler(this.cookieManager);
    }
    
    public static ConnectionManager getInstance() {
        return ConnectionManager.instance;
    }
    
    public OkHttpClient getClient() {
        return this.client;
    }
    
    static {
        instance = new ConnectionManager();
    }
}

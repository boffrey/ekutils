package servconn.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

public class Constants
{
    public static final String ARK_URL = "http://mobile.arcgames.com/";
    public static final String EK_MASTER_URL = "http://master.ek.ifreeteam.com/";
    public static final String[] SET_VALUES;
    public static final Set<String> IGNOREUSERCARDS;
    public static final Map<String, String> SERVERLIST;
    public static final String NL;
    
    static {
        SET_VALUES = new String[] { "110000000", "110000001", "110000002" };
        IGNOREUSERCARDS = new HashSet<String>(Arrays.asList(Constants.SET_VALUES));
        (SERVERLIST = new HashMap<String, String>()).put("equilibrium", "http://s1.ek.fedeengames.com");
        Constants.SERVERLIST.put("aeon", "http://s2.ek.fedeengames.com");
        Constants.SERVERLIST.put("ethereal", "http://s3.ek.fedeen.com");
        Constants.SERVERLIST.put("ascension", "http://s4.ek.ifreeteam.com");
        Constants.SERVERLIST.put("skorn", "http://s1.ekru.ifreeteam.com");
        Constants.SERVERLIST.put("apollo", "http://s1.ekbb.ifreeteam.com");
        NL = System.getProperty("line.separator");
    }
}

// 
// EkUtils 
// 

package servconn.sim;

import servconn.dto.battle.BattleRune;
import servconn.dto.battle.BattleCard;
import servconn.dto.battle.Player;
import servconn.dto.league.User;
import servconn.dto.league.BattleInfo;
import java.util.Iterator;
import servconn.dto.league.LeagueNow;
import servconn.dto.league.Data;
import servconn.dto.league.RoundResult;
import java.util.List;
import servconn.util.Constants;
import servconn.dto.league.LeagueData;
import servconn.dto.skill.Skill;
import servconn.dto.card.Card;
import servconn.dto.rune.Rune;
import java.util.Map;
import java.util.TreeMap;

public class MitziSimFoHPrinter implements FoHPrinter
{
    public static TreeMap<String, String> evoNames;
    private Map<String, Rune> runeMap;
    private Map<String, Card> cardMap;
    private Map<String, Skill> skillMap;
    
    @Override
    public String printDeck(final LeagueData leagueData) {
        if (leagueData == null || leagueData.getData() == null || leagueData.getData().getLeagueNow() == null) {
            throw new IllegalArgumentException("League Data format error");
        }
        this.checkArguments();
        final Data data = leagueData.getData();
        final LeagueNow leagueNow = data.getLeagueNow();
        final StringBuilder deckBuilder = new StringBuilder();
        deckBuilder.append("60").append(Constants.NL).append("60").append(Constants.NL).append(Constants.NL);
        final List<List<RoundResult>> roundResults = leagueNow.getRoundResult();
        final Iterator<List<RoundResult>> iterator = roundResults.iterator();
        if (iterator.hasNext()) {
            final List<RoundResult> roundResultList = iterator.next();
            int deckNo = 0;
            for (final RoundResult roundResult : roundResultList) {
                ++deckNo;
                final BattleInfo battleInfo = roundResult.getBattleInfo();
                final User user = battleInfo.getUser();
                deckBuilder.append(user.getNickName()).append(Constants.NL);
                deckBuilder.append(user.getLevel()).append(Constants.NL);
                final List<servconn.dto.league.Card> cardList = battleInfo.getCards();
                for (final servconn.dto.league.Card cardRef : cardList) {
                    deckBuilder.append(this.getCardInfoAsString(cardRef)).append(Constants.NL);
                }
                final List<servconn.dto.league.Rune> runeList = battleInfo.getRunes();
                for (final servconn.dto.league.Rune runeRef : runeList) {
                    deckBuilder.append(this.getRuneInfoAsString(runeRef)).append(Constants.NL);
                }
                if (deckNo != 8) {
                    deckBuilder.append(Constants.NL);
                }
            }
        }
        return deckBuilder.toString();
    }
    
    @Override
    public String printDeck(final Player player) {
        if (player == null) {
            throw new IllegalArgumentException("Player error");
        }
        this.checkArguments();
        final StringBuilder deckBuilder = new StringBuilder();
        deckBuilder.append(player.getNickName()).append(Constants.NL);
        deckBuilder.append(player.getLevel()).append(Constants.NL);
        final List<BattleCard> cardList = player.getCards();
        for (final BattleCard card : cardList) {
            if (!Constants.IGNOREUSERCARDS.contains(card.getUserCardId())) {
                deckBuilder.append(this.getCardInfoAsString(card)).append(Constants.NL);
            }
        }
        final List<BattleRune> runeList = player.getRunes();
        for (final BattleRune rune : runeList) {
            deckBuilder.append(this.getRuneInfoAsString(rune)).append(Constants.NL);
        }
        return deckBuilder.toString();
    }
    
    private String getRuneInfoAsString(final BattleRune rune) {
        return this.getRuneInfoAsString(rune.getRuneId(), rune.getLevel());
    }
    
    private String getRuneInfoAsString(final servconn.dto.league.Rune runeRef) {
        return this.getRuneInfoAsString(runeRef.getRuneId(), runeRef.getLevel());
    }
    
    private String getRuneInfoAsString(final String runeId, final String runeLevel) {
        final StringBuilder sb = new StringBuilder();
        final Rune rune = this.runeMap.get(runeId);
        if (rune != null) {
            sb.append("Rune: ").append(rune.getRuneName()).append(";").append(runeLevel);
            return sb.toString();
        }
        return "";
    }
    
    private String getCardInfoAsString(final BattleCard card) {
        return this.getCardInfoAsString(card.getCardId(), card.getLevel(), card.getEvolution(), card.getSkillNew());
    }
    
    private String getCardInfoAsString(final servconn.dto.league.Card cardRef) {
        return this.getCardInfoAsString(cardRef.getCardId(), cardRef.getLevel(), cardRef.getEvolution(), cardRef.getSkillNew());
    }
    
    private String getCardInfoAsString(final String cardId, final String level, final String evolution, final String skillNew) {
        final StringBuilder sb = new StringBuilder();
        final Card card = this.cardMap.get(cardId);
        sb.append(card.getCardName());
        sb.append(";").append(level).append(";").append(this.getCardCost(evolution));
        if (!skillNew.equals("0")) {
            sb.append(";");
            sb.append(this.convertSkillName(this.skillMap.get(skillNew).getName()));
        }
        return sb.toString();
    }
    
    private Integer getCardCost(final String evoLevel) {
        final Integer eLevel = Integer.parseInt(evoLevel);
        Integer evoCost = 0;
        switch (eLevel) {
            case 0: {
                evoCost = 0;
                break;
            }
            case 1:
            case 2: {
                evoCost = 1;
                break;
            }
            case 3:
            case 4: {
                evoCost = 2;
                break;
            }
            default: {
                evoCost = 3;
                break;
            }
        }
        return evoCost;
    }
    
    @Override
    public void setGameInfo(final Map<String, Card> cardMap, final Map<String, Skill> skillMap, final Map<String, Rune> runeMap) {
        this.cardMap = cardMap;
        this.skillMap = skillMap;
        this.runeMap = runeMap;
    }
    
    @Override
    public void setEvoNames(final Object evoNames) {
        MitziSimFoHPrinter.evoNames = (TreeMap<String, String>)evoNames;
    }
    
    public String convertSkillName(final String sName) {
        if (sName == null || "".equals(sName)) {
            return "";
        }
        final String skillName = sName.trim();
        if (MitziSimFoHPrinter.evoNames == null || MitziSimFoHPrinter.evoNames.isEmpty()) {
            return skillName;
        }
        final String skillLevel = this.getSkillLevel(skillName);
        String skillMitzi = "";
        if ("".equals(skillLevel)) {
            skillMitzi = skillName;
        }
        else {
            skillMitzi = skillName.substring(0, skillName.lastIndexOf(skillLevel) - 1);
        }
        if (skillMitzi.contains("Quick Strike")) {
            skillMitzi = "QS: " + skillMitzi.replace("[Quick Strike]", "").replace("Quick Strike:", "").trim();
        }
        else if (skillMitzi.contains("Desperation")) {
            skillMitzi = "D: " + skillMitzi.replace("[Desperation]", "").replace("Desperation:", "").trim();
        }
        if (MitziSimFoHPrinter.evoNames.containsKey(skillMitzi)) {
            return MitziSimFoHPrinter.evoNames.get(skillMitzi) + skillLevel;
        }
        return "";
    }
    
    private String getSkillLevel(final String skillName) {
        final String level = skillName.substring(skillName.lastIndexOf(" ") + 1, skillName.length());
        if (this.isNumeric(level)) {
            return level;
        }
        return "";
    }
    
    public boolean isNumeric(final CharSequence cs) {
        for (int sz = cs.length(), i = 0; i < sz; ++i) {
            if (!Character.isDigit(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    private void checkArguments() {
        if (this.runeMap == null || this.cardMap == null || this.skillMap == null) {
            throw new IllegalArgumentException("GameInfo (cards, runes, skills) needed");
        }
        if (MitziSimFoHPrinter.evoNames == null) {
            throw new IllegalArgumentException("EvoNames needed for skill mapping");
        }
    }
}

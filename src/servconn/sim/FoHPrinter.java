// 
// EkUtils 
// 

package servconn.sim;

import servconn.dto.rune.Rune;
import servconn.dto.skill.Skill;
import servconn.dto.card.Card;
import java.util.Map;
import servconn.dto.battle.Player;
import servconn.dto.league.LeagueData;

public interface FoHPrinter
{
    String printDeck(final LeagueData p0);
    
    String printDeck(final Player p0);
    
    void setGameInfo(final Map<String, Card> p0, final Map<String, Skill> p1, final Map<String, Rune> p2);
    
    void setEvoNames(final Object p0);
}

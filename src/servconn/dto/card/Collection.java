// 
// EkUtils 
// 

package servconn.dto.card;

import servconn.dto.common.Version;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Collection
{
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private CollectionData data;
    @SerializedName("version")
    @Expose
    private Version version;
    
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(final Integer status) {
        this.status = status;
    }
    
    public CollectionData getData() {
        return this.data;
    }
    
    public void setData(final CollectionData data) {
        this.data = data;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public void setVersion(final Version version) {
        this.version = version;
    }
}

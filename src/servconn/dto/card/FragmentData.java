// 
// EkUtils 
// 

package servconn.dto.card;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class FragmentData
{
    @Expose
    private List<CardFragment> CardChips;
    
    public FragmentData() {
        this.CardChips = new ArrayList<CardFragment>();
    }
    
    public List<CardFragment> getCardChips() {
        return this.CardChips;
    }
    
    public void setCardChips(final List<CardFragment> CardChips) {
        this.CardChips = CardChips;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

// 
// EkUtils 
// 

package servconn.dto.card;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardCollect
{
    @SerializedName("cardId")
    @Expose
    private Integer cardId;
    @SerializedName("CollectNum")
    @Expose
    private Integer CollectNum;
    
    public Integer getCardId() {
        return this.cardId;
    }
    
    public void setCardId(final Integer cardId) {
        this.cardId = cardId;
    }
    
    public Integer getCollectNum() {
        return this.CollectNum;
    }
    
    public void setCollectNum(final Integer CollectNum) {
        this.CollectNum = CollectNum;
    }
}

// 
// EkUtils 
// 

package servconn.dto.hydra;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class FriendPointsList
{
    @Expose
    private Integer friendId;
    @Expose
    private String contributePoint;
    @Expose
    private String nickName;
    @Expose
    private String avatar;
    @Expose
    private String sex;
    @Expose
    private Integer level;
    @Expose
    private String totalPoint;
    
    public Integer getFriendId() {
        return this.friendId;
    }
    
    public void setFriendId(final Integer friendId) {
        this.friendId = friendId;
    }
    
    public String getContributePoint() {
        return this.contributePoint;
    }
    
    public void setContributePoint(final String contributePoint) {
        this.contributePoint = contributePoint;
    }
    
    public String getNickName() {
        return this.nickName;
    }
    
    public void setNickName(final String nickName) {
        this.nickName = nickName;
    }
    
    public String getAvatar() {
        return this.avatar;
    }
    
    public void setAvatar(final String avatar) {
        this.avatar = avatar;
    }
    
    public String getSex() {
        return this.sex;
    }
    
    public void setSex(final String sex) {
        this.sex = sex;
    }
    
    public Integer getLevel() {
        return this.level;
    }
    
    public void setLevel(final Integer level) {
        this.level = level;
    }
    
    public String getTotalPoint() {
        return this.totalPoint;
    }
    
    public void setTotalPoint(final String totalPoint) {
        this.totalPoint = totalPoint;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

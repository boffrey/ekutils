// 
// EkUtils 
// 

package servconn.dto.hydra;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class LegionHydra
{
    @Expose
    private String Name;
    @Expose
    private String Force;
    @Expose
    private String HeadId;
    @Expose
    private String Emblem;
    @Expose
    private Integer Level;
    @Expose
    private String Descr1;
    @Expose
    private Integer rank;
    @Expose
    private Integer Belong;
    
    public String getName() {
        return this.Name;
    }
    
    public void setName(final String Name) {
        this.Name = Name;
    }
    
    public String getForce() {
        return this.Force;
    }
    
    public void setForce(final String Force) {
        this.Force = Force;
    }
    
    public String getHeadId() {
        return this.HeadId;
    }
    
    public void setHeadId(final String HeadId) {
        this.HeadId = HeadId;
    }
    
    public String getEmblem() {
        return this.Emblem;
    }
    
    public void setEmblem(final String Emblem) {
        this.Emblem = Emblem;
    }
    
    public Integer getLevel() {
        return this.Level;
    }
    
    public void setLevel(final Integer Level) {
        this.Level = Level;
    }
    
    public String getDescr1() {
        return this.Descr1;
    }
    
    public void setDescr1(final String Descr1) {
        this.Descr1 = Descr1;
    }
    
    public Integer getRank() {
        return this.rank;
    }
    
    public void setRank(final Integer rank) {
        this.rank = rank;
    }
    
    public Integer getBelong() {
        return this.Belong;
    }
    
    public void setBelong(final Integer Belong) {
        this.Belong = Belong;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

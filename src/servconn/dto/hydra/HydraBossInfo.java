// 
// EkUtils 
// 

package servconn.dto.hydra;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class HydraBossInfo
{
    @Expose
    private String NickName;
    @Expose
    private Integer Grade;
    
    public String getNickName() {
        return this.NickName;
    }
    
    public void setNickName(final String NickName) {
        this.NickName = NickName;
    }
    
    public Integer getGrade() {
        return this.Grade;
    }
    
    public void setGrade(final Integer Grade) {
        this.Grade = Grade;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

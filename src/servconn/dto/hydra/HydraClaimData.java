// 
// EkUtils 
// 

package servconn.dto.hydra;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class HydraClaimData
{
    @Expose
    private HydraPointAward pointAward;
    @Expose
    private HydraBossInfo bossInfo;
    @Expose
    private List<Object> achievedPointsTarget;
    
    public HydraClaimData() {
        this.achievedPointsTarget = new ArrayList<Object>();
    }
    
    public HydraPointAward getPointAward() {
        return this.pointAward;
    }
    
    public void setPointAward(final HydraPointAward pointAward) {
        this.pointAward = pointAward;
    }
    
    public HydraBossInfo getBossInfo() {
        return this.bossInfo;
    }
    
    public void setBossInfo(final HydraBossInfo bossInfo) {
        this.bossInfo = bossInfo;
    }
    
    public List<Object> getAchievedPointsTarget() {
        return this.achievedPointsTarget;
    }
    
    public void setAchievedPointsTarget(final List<Object> achievedPointsTarget) {
        this.achievedPointsTarget = achievedPointsTarget;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

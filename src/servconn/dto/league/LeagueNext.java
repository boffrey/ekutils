// 
// EkUtils 
// 

package servconn.dto.league;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class LeagueNext
{
    @Expose
    private Integer LeagueId;
    @Expose
    private Integer ConditionId;
    @Expose
    private Integer RoundNow;
    @Expose
    private List<Object> RoundResult;
    @Expose
    private Integer Status;
    @Expose
    private Integer CreateTime;
    @Expose
    private Condition_ Condition;
    @Expose
    private Integer RemainTime;
    @Expose
    private List<Object> BattleInfo;
    
    public LeagueNext() {
        this.RoundResult = new ArrayList<Object>();
        this.BattleInfo = new ArrayList<Object>();
    }
    
    public Integer getLeagueId() {
        return this.LeagueId;
    }
    
    public void setLeagueId(final Integer LeagueId) {
        this.LeagueId = LeagueId;
    }
    
    public Integer getConditionId() {
        return this.ConditionId;
    }
    
    public void setConditionId(final Integer ConditionId) {
        this.ConditionId = ConditionId;
    }
    
    public Integer getRoundNow() {
        return this.RoundNow;
    }
    
    public void setRoundNow(final Integer RoundNow) {
        this.RoundNow = RoundNow;
    }
    
    public List<Object> getRoundResult() {
        return this.RoundResult;
    }
    
    public void setRoundResult(final List<Object> RoundResult) {
        this.RoundResult = RoundResult;
    }
    
    public Integer getStatus() {
        return this.Status;
    }
    
    public void setStatus(final Integer Status) {
        this.Status = Status;
    }
    
    public Integer getCreateTime() {
        return this.CreateTime;
    }
    
    public void setCreateTime(final Integer CreateTime) {
        this.CreateTime = CreateTime;
    }
    
    public Condition_ getCondition() {
        return this.Condition;
    }
    
    public void setCondition(final Condition_ Condition) {
        this.Condition = Condition;
    }
    
    public Integer getRemainTime() {
        return this.RemainTime;
    }
    
    public void setRemainTime(final Integer RemainTime) {
        this.RemainTime = RemainTime;
    }
    
    public List<Object> getBattleInfo() {
        return this.BattleInfo;
    }
    
    public void setBattleInfo(final List<Object> BattleInfo) {
        this.BattleInfo = BattleInfo;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

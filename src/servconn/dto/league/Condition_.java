// 
// EkUtils 
// 

package servconn.dto.league;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class Condition_
{
    @Expose
    private String Type;
    @Expose
    private List<Integer> Value;
    @Expose
    private String Desc;
    
    public Condition_() {
        this.Value = new ArrayList<Integer>();
    }
    
    public String getType() {
        return this.Type;
    }
    
    public void setType(final String Type) {
        this.Type = Type;
    }
    
    public List<Integer> getValue() {
        return this.Value;
    }
    
    public void setValue(final List<Integer> Value) {
        this.Value = Value;
    }
    
    public String getDesc() {
        return this.Desc;
    }
    
    public void setDesc(final String Desc) {
        this.Desc = Desc;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

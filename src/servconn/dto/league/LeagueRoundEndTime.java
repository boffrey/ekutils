// 
// EkUtils 
// 

package servconn.dto.league;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeagueRoundEndTime
{
    @SerializedName("1")
    @Expose
    private Integer _1;
    @SerializedName("2")
    @Expose
    private Integer _2;
    @SerializedName("3")
    @Expose
    private Integer _3;
    
    public Integer get1() {
        return this._1;
    }
    
    public void set1(final Integer _1) {
        this._1 = _1;
    }
    
    public Integer get2() {
        return this._2;
    }
    
    public void set2(final Integer _2) {
        this._2 = _2;
    }
    
    public Integer get3() {
        return this._3;
    }
    
    public void set3(final Integer _3) {
        this._3 = _3;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

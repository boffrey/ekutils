// 
// EkUtils 
// 

package servconn.dto.league;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class User
{
    @Expose
    private Integer Uid;
    @Expose
    private String NickName;
    @Expose
    private String Sex;
    @Expose
    private String Avatar;
    @Expose
    private String HP;
    @Expose
    private String Level;
    
    public Integer getUid() {
        return this.Uid;
    }
    
    public void setUid(final Integer Uid) {
        this.Uid = Uid;
    }
    
    public String getNickName() {
        return this.NickName;
    }
    
    public void setNickName(final String NickName) {
        this.NickName = NickName;
    }
    
    public String getSex() {
        return this.Sex;
    }
    
    public void setSex(final String Sex) {
        this.Sex = Sex;
    }
    
    public String getAvatar() {
        return this.Avatar;
    }
    
    public void setAvatar(final String Avatar) {
        this.Avatar = Avatar;
    }
    
    public String getHP() {
        return this.HP;
    }
    
    public void setHP(final String HP) {
        this.HP = HP;
    }
    
    public String getLevel() {
        return this.Level;
    }
    
    public void setLevel(final String Level) {
        this.Level = Level;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

// 
// EkUtils 
// 

package servconn.dto.buy;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.Arrays;
import java.util.List;
import com.google.gson.annotations.Expose;

public class BuyingResult
{
    @Expose
    private String CardIds;
    @Expose
    private Integer CoinsAfter;
    @Expose
    private Integer CashAfter;
    @Expose
    private Integer TicketAfter;
    
    public List<String> getCardIdsAsList() {
        return Arrays.asList(this.CardIds.split("_"));
    }
    
    public String getCardIds() {
        return this.CardIds;
    }
    
    public void setCardIds(final String CardIds) {
        this.CardIds = CardIds;
    }
    
    public Integer getCoinsAfter() {
        return this.CoinsAfter;
    }
    
    public void setCoinsAfter(final Integer CoinsAfter) {
        this.CoinsAfter = CoinsAfter;
    }
    
    public Integer getCashAfter() {
        return this.CashAfter;
    }
    
    public void setCashAfter(final Integer CashAfter) {
        this.CashAfter = CashAfter;
    }
    
    public Integer getTicketAfter() {
        return this.TicketAfter;
    }
    
    public void setTicketAfter(final Integer TicketAfter) {
        this.TicketAfter = TicketAfter;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

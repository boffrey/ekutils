// 
// EkUtils 
// 

package servconn.dto.battle;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class BattleCard
{
    @Expose
    private String UUID;
    @Expose
    private String CardId;
    @Expose
    private String UserCardId;
    @Expose
    private Integer Attack;
    @Expose
    private Integer HP;
    @Expose
    private String Wait;
    @Expose
    private String Level;
    @Expose
    private String SkillNew;
    @Expose
    private String Evolution;
    @Expose
    private Object WashTime;
    
    public String getUUID() {
        return this.UUID;
    }
    
    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }
    
    public String getCardId() {
        return this.CardId;
    }
    
    public void setCardId(final String CardId) {
        this.CardId = CardId;
    }
    
    public String getUserCardId() {
        return this.UserCardId;
    }
    
    public void setUserCardId(final String UserCardId) {
        this.UserCardId = UserCardId;
    }
    
    public Integer getAttack() {
        return this.Attack;
    }
    
    public void setAttack(final Integer Attack) {
        this.Attack = Attack;
    }
    
    public Integer getHP() {
        return this.HP;
    }
    
    public void setHP(final Integer HP) {
        this.HP = HP;
    }
    
    public String getWait() {
        return this.Wait;
    }
    
    public void setWait(final String Wait) {
        this.Wait = Wait;
    }
    
    public String getLevel() {
        return this.Level;
    }
    
    public void setLevel(final String Level) {
        this.Level = Level;
    }
    
    public String getSkillNew() {
        return this.SkillNew;
    }
    
    public void setSkillNew(final String SkillNew) {
        this.SkillNew = SkillNew;
    }
    
    public String getEvolution() {
        return this.Evolution;
    }
    
    public void setEvolution(final String Evolution) {
        this.Evolution = Evolution;
    }
    
    public Object getWashTime() {
        return this.WashTime;
    }
    
    public void setWashTime(final Object WashTime) {
        this.WashTime = WashTime;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

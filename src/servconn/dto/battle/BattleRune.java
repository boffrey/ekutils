// 
// EkUtils 
// 

package servconn.dto.battle;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class BattleRune
{
    @Expose
    private String UUID;
    @Expose
    private String RuneId;
    @Expose
    private String UserRuneId;
    @Expose
    private String Level;
    
    public String getUUID() {
        return this.UUID;
    }
    
    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }
    
    public String getRuneId() {
        return this.RuneId;
    }
    
    public void setRuneId(final String RuneId) {
        this.RuneId = RuneId;
    }
    
    public String getUserRuneId() {
        return this.UserRuneId;
    }
    
    public void setUserRuneId(final String UserRuneId) {
        this.UserRuneId = UserRuneId;
    }
    
    public String getLevel() {
        return this.Level;
    }
    
    public void setLevel(final String Level) {
        this.Level = Level;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

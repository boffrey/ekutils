// 
// EkUtils 
// 

package servconn.dto.battle;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class Data
{
    @Expose
    private String BattleId;
    @Expose
    private Integer Win;
    @Expose
    private List<Object> ExtData;
    @Expose
    private Object prepare;
    @Expose
    private Player AttackPlayer;
    @Expose
    private Player DefendPlayer;
    private transient List<Battle> Battle;
    
    public Data() {
        this.ExtData = new ArrayList<Object>();
        this.Battle = new ArrayList<Battle>();
    }
    
    public String getBattleId() {
        return this.BattleId;
    }
    
    public void setBattleId(final String BattleId) {
        this.BattleId = BattleId;
    }
    
    public Integer getWin() {
        return this.Win;
    }
    
    public void setWin(final Integer Win) {
        this.Win = Win;
    }
    
    public List<Object> getExtData() {
        return this.ExtData;
    }
    
    public void setExtData(final List<Object> ExtData) {
        this.ExtData = ExtData;
    }
    
    public Object getPrepare() {
        return this.prepare;
    }
    
    public void setPrepare(final Object prepare) {
        this.prepare = prepare;
    }
    
    public Player getAttackPlayer() {
        return this.AttackPlayer;
    }
    
    public void setAttackPlayer(final Player AttackPlayer) {
        this.AttackPlayer = AttackPlayer;
    }
    
    public Player getDefendPlayer() {
        return this.DefendPlayer;
    }
    
    public void setDefendPlayer(final Player DefendPlayer) {
        this.DefendPlayer = DefendPlayer;
    }
    
    public List<Battle> getBattle() {
        return this.Battle;
    }
    
    public void setBattle(final List<Battle> Battle) {
        this.Battle = Battle;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

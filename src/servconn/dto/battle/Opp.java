// 
// EkUtils 
// 

package servconn.dto.battle;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class Opp
{
    @Expose
    private String UUID;
    @Expose
    private Integer Opp;
    @Expose
    private List<String> Target;
    @Expose
    private Integer Value;
    @Expose
    private Integer HP;
    
    public Opp() {
        this.Target = new ArrayList<String>();
    }
    
    public String getUUID() {
        return this.UUID;
    }
    
    public void setUUID(final String UUID) {
        this.UUID = UUID;
    }
    
    public Integer getOpp() {
        return this.Opp;
    }
    
    public void setOpp(final Integer Opp) {
        this.Opp = Opp;
    }
    
    public List<String> getTarget() {
        return this.Target;
    }
    
    public void setTarget(final List<String> Target) {
        this.Target = Target;
    }
    
    public Integer getValue() {
        return this.Value;
    }
    
    public void setValue(final Integer Value) {
        this.Value = Value;
    }
    
    public Integer getHP() {
        return this.HP;
    }
    
    public void setHP(final Integer HP) {
        this.HP = HP;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

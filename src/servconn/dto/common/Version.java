// 
// EkUtils 
// 

package servconn.dto.common;

import com.google.gson.annotations.Expose;

public class Version
{
    @Expose
    private String http;
    @Expose
    private String stop;
    @Expose
    private String appversion;
    @Expose
    private String appurl;
    
    public String getHttp() {
        return this.http;
    }
    
    public void setHttp(final String http) {
        this.http = http;
    }
    
    public String getStop() {
        return this.stop;
    }
    
    public void setStop(final String stop) {
        this.stop = stop;
    }
    
    public String getAppversion() {
        return this.appversion;
    }
    
    public void setAppversion(final String appversion) {
        this.appversion = appversion;
    }
    
    public String getAppurl() {
        return this.appurl;
    }
    
    public void setAppurl(final String appurl) {
        this.appurl = appurl;
    }
}

// 
// EkUtils 
// 

package servconn.dto.hydralist;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class HydraList
{
    @Expose
    private Integer UserHydraId;
    @Expose
    private Integer Uid;
    @Expose
    private String HydraId;
    @Expose
    private String NickName;
    @Expose
    private String Avatar;
    @Expose
    private String Sex;
    @Expose
    private Integer Status;
    @Expose
    private String LastAttackerName;
    @Expose
    private String LastAttackerUid;
    @Expose
    private Integer HPCount;
    @Expose
    private Integer HPCurrent;
    @Expose
    private Integer Round;
    @Expose
    private Integer Grade;
    @Expose
    private Integer Level;
    @Expose
    private String HydraName;
    @Expose
    private String HydraAvatar;
    @Expose
    private String HydraBigAvatar;
    @Expose
    private Integer Time;
    @Expose
    private Integer FleeTime;
    @Expose
    private Integer enableAward;
    
    public Integer getUserHydraId() {
        return this.UserHydraId;
    }
    
    public void setUserHydraId(final Integer UserHydraId) {
        this.UserHydraId = UserHydraId;
    }
    
    public Integer getUid() {
        return this.Uid;
    }
    
    public void setUid(final Integer Uid) {
        this.Uid = Uid;
    }
    
    public String getHydraId() {
        return this.HydraId;
    }
    
    public void setHydraId(final String HydraId) {
        this.HydraId = HydraId;
    }
    
    public String getNickName() {
        return this.NickName;
    }
    
    public void setNickName(final String NickName) {
        this.NickName = NickName;
    }
    
    public String getAvatar() {
        return this.Avatar;
    }
    
    public void setAvatar(final String Avatar) {
        this.Avatar = Avatar;
    }
    
    public String getSex() {
        return this.Sex;
    }
    
    public void setSex(final String Sex) {
        this.Sex = Sex;
    }
    
    public Integer getStatus() {
        return this.Status;
    }
    
    public void setStatus(final Integer Status) {
        this.Status = Status;
    }
    
    public String getLastAttackerName() {
        return this.LastAttackerName;
    }
    
    public void setLastAttackerName(final String LastAttackerName) {
        this.LastAttackerName = LastAttackerName;
    }
    
    public String getLastAttackerUid() {
        return this.LastAttackerUid;
    }
    
    public void setLastAttackerUid(final String LastAttackerUid) {
        this.LastAttackerUid = LastAttackerUid;
    }
    
    public Integer getHPCount() {
        return this.HPCount;
    }
    
    public void setHPCount(final Integer HPCount) {
        this.HPCount = HPCount;
    }
    
    public Integer getHPCurrent() {
        return this.HPCurrent;
    }
    
    public void setHPCurrent(final Integer HPCurrent) {
        this.HPCurrent = HPCurrent;
    }
    
    public Integer getRound() {
        return this.Round;
    }
    
    public void setRound(final Integer Round) {
        this.Round = Round;
    }
    
    public Integer getGrade() {
        return this.Grade;
    }
    
    public void setGrade(final Integer Grade) {
        this.Grade = Grade;
    }
    
    public Integer getLevel() {
        return this.Level;
    }
    
    public void setLevel(final Integer Level) {
        this.Level = Level;
    }
    
    public String getHydraName() {
        return this.HydraName;
    }
    
    public void setHydraName(final String HydraName) {
        this.HydraName = HydraName;
    }
    
    public String getHydraAvatar() {
        return this.HydraAvatar;
    }
    
    public void setHydraAvatar(final String HydraAvatar) {
        this.HydraAvatar = HydraAvatar;
    }
    
    public String getHydraBigAvatar() {
        return this.HydraBigAvatar;
    }
    
    public void setHydraBigAvatar(final String HydraBigAvatar) {
        this.HydraBigAvatar = HydraBigAvatar;
    }
    
    public Integer getTime() {
        return this.Time;
    }
    
    public void setTime(final Integer Time) {
        this.Time = Time;
    }
    
    public Integer getFleeTime() {
        return this.FleeTime;
    }
    
    public void setFleeTime(final Integer FleeTime) {
        this.FleeTime = FleeTime;
    }
    
    public Integer getEnableAward() {
        return this.enableAward;
    }
    
    public void setEnableAward(final Integer enableAward) {
        this.enableAward = enableAward;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

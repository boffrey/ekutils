// 
// EkUtils 
// 

package servconn.dto.foh;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class TavernData
{
    @SerializedName("Result")
    @Expose
    private List<String> Result;
    @SerializedName("Award")
    @Expose
    private List<TavernAward> Award;
    @SerializedName("LotteryCash")
    @Expose
    private Integer LotteryCash;
    
    public TavernData() {
        this.Result = new ArrayList<String>();
    }
    
    public List<String> getResult() {
        return this.Result;
    }
    
    public void setResult(final List<String> Result) {
        this.Result = Result;
    }
    
    public List<TavernAward> getAward() {
        return this.Award;
    }
    
    public void setAward(final List<TavernAward> Award) {
        this.Award = Award;
    }
    
    public Integer getLotteryCash() {
        return this.LotteryCash;
    }
    
    public void setLotteryCash(final Integer LotteryCash) {
        this.LotteryCash = LotteryCash;
    }
}

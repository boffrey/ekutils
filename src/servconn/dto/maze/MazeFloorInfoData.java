// 
// EkUtils 
// 

package servconn.dto.maze;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class MazeFloorInfoData
{
    @Expose
    private String Name;
    @Expose
    private Integer BoxNum;
    @Expose
    private Integer MonsterNum;
    @Expose
    private Integer RemainBoxNum;
    @Expose
    private Integer RemainMonsterNum;
    @Expose
    private Integer Layer;
    @Expose
    private Integer TotalLayer;
    @Expose
    private MazeFloorMap Map;
    
    public String getName() {
        return this.Name;
    }
    
    public void setName(final String Name) {
        this.Name = Name;
    }
    
    public Integer getBoxNum() {
        return this.BoxNum;
    }
    
    public void setBoxNum(final Integer BoxNum) {
        this.BoxNum = BoxNum;
    }
    
    public Integer getMonsterNum() {
        return this.MonsterNum;
    }
    
    public void setMonsterNum(final Integer MonsterNum) {
        this.MonsterNum = MonsterNum;
    }
    
    public Integer getRemainBoxNum() {
        return this.RemainBoxNum;
    }
    
    public void setRemainBoxNum(final Integer RemainBoxNum) {
        this.RemainBoxNum = RemainBoxNum;
    }
    
    public Integer getRemainMonsterNum() {
        return this.RemainMonsterNum;
    }
    
    public void setRemainMonsterNum(final Integer RemainMonsterNum) {
        this.RemainMonsterNum = RemainMonsterNum;
    }
    
    public Integer getLayer() {
        return this.Layer;
    }
    
    public void setLayer(final Integer Layer) {
        this.Layer = Layer;
    }
    
    public Integer getTotalLayer() {
        return this.TotalLayer;
    }
    
    public void setTotalLayer(final Integer TotalLayer) {
        this.TotalLayer = TotalLayer;
    }
    
    public MazeFloorMap getMap() {
        return this.Map;
    }
    
    public void setMap(final MazeFloorMap Map) {
        this.Map = Map;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

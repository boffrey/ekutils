// 
// EkUtils 
// 

package servconn.dto.maze;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class MazeFloorMap
{
    @Expose
    private Boolean IsFinish;
    @Expose
    private List<Integer> WallRows;
    @Expose
    private List<Integer> WallCols;
    @Expose
    private List<Integer> Items;
    
    public MazeFloorMap() {
        this.WallRows = new ArrayList<Integer>();
        this.WallCols = new ArrayList<Integer>();
        this.Items = new ArrayList<Integer>();
    }
    
    public Boolean getIsFinish() {
        return this.IsFinish;
    }
    
    public void setIsFinish(final Boolean IsFinish) {
        this.IsFinish = IsFinish;
    }
    
    public List<Integer> getWallRows() {
        return this.WallRows;
    }
    
    public void setWallRows(final List<Integer> WallRows) {
        this.WallRows = WallRows;
    }
    
    public List<Integer> getWallCols() {
        return this.WallCols;
    }
    
    public void setWallCols(final List<Integer> WallCols) {
        this.WallCols = WallCols;
    }
    
    public List<Integer> getItems() {
        return this.Items;
    }
    
    public void setItems(final List<Integer> Items) {
        this.Items = Items;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

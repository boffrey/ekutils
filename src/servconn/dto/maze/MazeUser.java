// 
// EkUtils 
// 

package servconn.dto.maze;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class MazeUser
{
    @Expose
    private String Level;
    @Expose
    private Integer Exp;
    @Expose
    private Integer PrevExp;
    @Expose
    private Integer NextExp;
    
    public String getLevel() {
        return this.Level;
    }
    
    public void setLevel(final String Level) {
        this.Level = Level;
    }
    
    public Integer getExp() {
        return this.Exp;
    }
    
    public void setExp(final Integer Exp) {
        this.Exp = Exp;
    }
    
    public Integer getPrevExp() {
        return this.PrevExp;
    }
    
    public void setPrevExp(final Integer PrevExp) {
        this.PrevExp = PrevExp;
    }
    
    public Integer getNextExp() {
        return this.NextExp;
    }
    
    public void setNextExp(final Integer NextExp) {
        this.NextExp = NextExp;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

// 
// EkUtils 
// 

package servconn.dto.maze;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class MazeAwardCardChip
{
    @Expose
    private String Id;
    @Expose
    private String Num;
    
    public String getId() {
        return this.Id;
    }
    
    public void setId(final String Id) {
        this.Id = Id;
    }
    
    public String getNum() {
        return this.Num;
    }
    
    public void setNum(final String Num) {
        this.Num = Num;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

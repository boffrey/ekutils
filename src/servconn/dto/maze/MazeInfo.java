// 
// EkUtils 
// 

package servconn.dto.maze;

import servconn.dto.common.Version;
import com.google.gson.annotations.Expose;

public class MazeInfo
{
    @Expose
    private Integer status;
    @Expose
    private MazeInfoData data;
    @Expose
    private Version version;
    
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(final Integer status) {
        this.status = status;
    }
    
    public MazeInfoData getData() {
        return this.data;
    }
    
    public void setData(final MazeInfoData data) {
        this.data = data;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public void setVersion(final Version version) {
        this.version = version;
    }
}

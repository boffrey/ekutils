// 
// EkUtils 
// 

package servconn.dto.meditate;

import org.apache.commons.lang3.builder.ToStringBuilder;
import servconn.dto.common.Version;
import com.google.gson.annotations.Expose;

public class MeditateMultiple
{
    @Expose
    private MeditateMultipleData data;
    @Expose
    private Integer status;
    @Expose
    private Version version;
    
    public MeditateMultipleData getData() {
        return this.data;
    }
    
    public void setData(final MeditateMultipleData data) {
        this.data = data;
    }
    
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(final Integer status) {
        this.status = status;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public void setVersion(final Version version) {
        this.version = version;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

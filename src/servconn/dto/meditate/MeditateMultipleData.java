// 
// EkUtils 
// 

package servconn.dto.meditate;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class MeditateMultipleData
{
    @Expose
    private List<MeditateAwardItem> AwardItem;
    @Expose
    private List<Integer> NpcList;
    @Expose
    private Integer Coins;
    @Expose
    private Integer OneKeyTimes;
    
    public MeditateMultipleData() {
        this.AwardItem = new ArrayList<MeditateAwardItem>();
        this.NpcList = new ArrayList<Integer>();
    }
    
    public List<MeditateAwardItem> getAwardItem() {
        return this.AwardItem;
    }
    
    public void setAwardItem(final List<MeditateAwardItem> AwardItem) {
        this.AwardItem = AwardItem;
    }
    
    public List<Integer> getNpcList() {
        return this.NpcList;
    }
    
    public void setNpcList(final List<Integer> NpcList) {
        this.NpcList = NpcList;
    }
    
    public Integer getCoins() {
        return this.Coins;
    }
    
    public void setCoins(final Integer Coins) {
        this.Coins = Coins;
    }
    
    public Integer getOneKeyTimes() {
        return this.OneKeyTimes;
    }
    
    public void setOneKeyTimes(final Integer OneKeyTimes) {
        this.OneKeyTimes = OneKeyTimes;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

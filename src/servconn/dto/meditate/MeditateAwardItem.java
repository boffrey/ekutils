// 
// EkUtils 
// 

package servconn.dto.meditate;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class MeditateAwardItem
{
    @Expose
    private Integer Type;
    @Expose
    private Integer Value;
    
    public Integer getType() {
        return this.Type;
    }
    
    public void setType(final Integer Type) {
        this.Type = Type;
    }
    
    public Integer getValue() {
        return this.Value;
    }
    
    public void setValue(final Integer Value) {
        this.Value = Value;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

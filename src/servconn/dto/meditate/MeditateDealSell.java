// 
// EkUtils 
// 

package servconn.dto.meditate;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class MeditateDealSell
{
    @Expose
    private Integer Type;
    @Expose
    private Integer Value;
    @Expose
    private Integer Num;
    
    public Integer getType() {
        return this.Type;
    }
    
    public void setType(final Integer Type) {
        this.Type = Type;
    }
    
    public Integer getValue() {
        return this.Value;
    }
    
    public void setValue(final Integer Value) {
        this.Value = Value;
    }
    
    public Integer getNum() {
        return this.Num;
    }
    
    public void setNum(final Integer Num) {
        this.Num = Num;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

// 
// EkUtils 
// 

package servconn.dto.skill;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class Data
{
    @Expose
    private List<Skill> Skills;
    
    public Data() {
        this.Skills = new ArrayList<Skill>();
    }
    
    public List<Skill> getSkills() {
        return this.Skills;
    }
    
    public void setSkills(final List<Skill> Skills) {
        this.Skills = Skills;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

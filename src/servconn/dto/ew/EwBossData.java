// 
// EkUtils 
// 

package servconn.dto.ew;

import org.apache.commons.lang3.builder.ToStringBuilder;
import servconn.dto.common.Version;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EwBossData
{
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private EwBoss data;
    @SerializedName("version")
    @Expose
    private Version version;
    
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(final Integer status) {
        this.status = status;
    }
    
    public EwBoss getData() {
        return this.data;
    }
    
    public void setData(final EwBoss data) {
        this.data = data;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public void setVersion(final Version version) {
        this.version = version;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

// 
// EkUtils 
// 

package servconn.dto.ew;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EwBossCard
{
    @SerializedName("CardId")
    @Expose
    private Integer CardId;
    @SerializedName("Level")
    @Expose
    private Integer Level;
    @SerializedName("Evolution")
    @Expose
    private Integer Evolution;
    @SerializedName("SkillNew")
    @Expose
    private Integer SkillNew;
    @SerializedName("WashTime")
    @Expose
    private Integer WashTime;
    @SerializedName("Hp")
    @Expose
    private Integer Hp;
    
    public Integer getCardId() {
        return this.CardId;
    }
    
    public void setCardId(final Integer CardId) {
        this.CardId = CardId;
    }
    
    public Integer getLevel() {
        return this.Level;
    }
    
    public void setLevel(final Integer Level) {
        this.Level = Level;
    }
    
    public Integer getEvolution() {
        return this.Evolution;
    }
    
    public void setEvolution(final Integer Evolution) {
        this.Evolution = Evolution;
    }
    
    public Integer getSkillNew() {
        return this.SkillNew;
    }
    
    public void setSkillNew(final Integer SkillNew) {
        this.SkillNew = SkillNew;
    }
    
    public Integer getWashTime() {
        return this.WashTime;
    }
    
    public void setWashTime(final Integer WashTime) {
        this.WashTime = WashTime;
    }
    
    public Integer getHp() {
        return this.Hp;
    }
    
    public void setHp(final Integer Hp) {
        this.Hp = Hp;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

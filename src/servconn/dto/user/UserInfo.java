// 
// EkUtils 
// 

package servconn.dto.user;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import java.util.Map;
import com.google.gson.annotations.Expose;

public class UserInfo
{
    @Expose
    private String Uid;
    @Expose
    private String PwdLockMessage;
    @Expose
    private String Sex;
    @Expose
    private String NickName;
    @Expose
    private String Avatar;
    @Expose
    private String RegisterTime;
    @Expose
    private Integer Win;
    @Expose
    private Integer Lose;
    @Expose
    private String Level;
    @Expose
    private Long Exp;
    @Expose
    private Integer Coins;
    @Expose
    private Integer Cash;
    @Expose
    private String Ticket;
    @Expose
    private Map<String, String> FreshStep;
    @Expose
    private Integer Energy;
    @Expose
    private Integer EnergyLastTime;
    @Expose
    private String EnergyBuyTime;
    @Expose
    private String EnergyBuyCount;
    @Expose
    private Integer EnergyMax;
    @Expose
    private String LeaderShip;
    @Expose
    private String FriendApplyNum;
    @Expose
    private String FriendNumMax;
    @Expose
    private Integer DefaultGroupId;
    @Expose
    private String RankWin;
    @Expose
    private String RankLost;
    @Expose
    private Integer RankTimes;
    @Expose
    private String ThievesTimes;
    @SerializedName("Fragment_5")
    @Expose
    private String Fragment5;
    @SerializedName("Fragment_4")
    @Expose
    private String Fragment4;
    @SerializedName("Fragment_3")
    @Expose
    private String Fragment3;
    @Expose
    private String InviteCode;
    @Expose
    private String InviteNum;
    @Expose
    private String Udid;
    @Expose
    private String LostPoint;
    @Expose
    private String Origin;
    @Expose
    private Object Platform;
    @Expose
    private Object Language;
    @Expose
    private String AchievementPoint;
    @Expose
    private String TotalDeposit;
    @Expose
    private Integer LeaguePoint;
    @Expose
    private List<Integer> CardMaterials;
    @Expose
    private String UserName;
    @Expose
    private String HP;
    @Expose
    private String PrevExp;
    @Expose
    private String NextExp;
    @Expose
    private Integer VipLevel;
    @Expose
    private Integer NewEmail;
    @Expose
    private Integer SalaryCount;
    @Expose
    private Integer LoginContinueTimesXX;
    @Expose
    private Integer maxMapStageId;
    @Expose
    private Integer EvolutionTimes;
    @Expose
    private Integer TaskDailyReward;
    @Expose
    private List<String> LockedUserCardIds;
    @Expose
    private Integer Paid;
    @Expose
    private HydraActStatus HydraActStatus;
    @Expose
    private Integer TreeActStatus;
    @Expose
    private Integer CountryWar;
    @Expose
    private Integer ElementWarActStatus;
    @Expose
    private Integer CardMakeStatus;
    @Expose
    private Integer Boss;
    @Expose
    private Integer CanGetAchievement;
    @Expose
    private Integer Regtime;
    @Expose
    private Integer TimeWelfare;
    @Expose
    private Integer BuyEnergyCost;
    @Expose
    private Integer showAD;
    @Expose
    private Integer ShowEvolve;
    @Expose
    private Integer showAcionView;
    @Expose
    private Integer shopFreeBuy;
    @Expose
    private String MaxCard;
    
    public UserInfo() {
        this.CardMaterials = new ArrayList<Integer>();
        this.LockedUserCardIds = new ArrayList<String>();
    }
    
    public String getUid() {
        return this.Uid;
    }
    
    public void setUid(final String Uid) {
        this.Uid = Uid;
    }
    
    public String getPwdLockMessage() {
        return this.PwdLockMessage;
    }
    
    public void setPwdLockMessage(final String PwdLockMessage) {
        this.PwdLockMessage = PwdLockMessage;
    }
    
    public String getSex() {
        return this.Sex;
    }
    
    public void setSex(final String Sex) {
        this.Sex = Sex;
    }
    
    public String getNickName() {
        return this.NickName;
    }
    
    public void setNickName(final String NickName) {
        this.NickName = NickName;
    }
    
    public String getAvatar() {
        return this.Avatar;
    }
    
    public void setAvatar(final String Avatar) {
        this.Avatar = Avatar;
    }
    
    public String getRegisterTime() {
        return this.RegisterTime;
    }
    
    public void setRegisterTime(final String RegisterTime) {
        this.RegisterTime = RegisterTime;
    }
    
    public Integer getWin() {
        return this.Win;
    }
    
    public void setWin(final Integer Win) {
        this.Win = Win;
    }
    
    public Integer getLose() {
        return this.Lose;
    }
    
    public void setLose(final Integer Lose) {
        this.Lose = Lose;
    }
    
    public String getLevel() {
        return this.Level;
    }
    
    public void setLevel(final String Level) {
        this.Level = Level;
    }
    
    public Long getExp() {
        return this.Exp;
    }
    
    public void setExp(final Long Exp) {
        this.Exp = Exp;
    }
    
    public Integer getCoins() {
        return this.Coins;
    }
    
    public void setCoins(final Integer Coins) {
        this.Coins = Coins;
    }
    
    public Integer getCash() {
        return this.Cash;
    }
    
    public void setCash(final Integer Cash) {
        this.Cash = Cash;
    }
    
    public String getTicket() {
        return this.Ticket;
    }
    
    public void setTicket(final String Ticket) {
        this.Ticket = Ticket;
    }
    
    public Map<String, String> getFreshStep() {
        return this.FreshStep;
    }
    
    public void setFreshStep(final Map<String, String> freshStep) {
        this.FreshStep = freshStep;
    }
    
    public Integer getEnergy() {
        return this.Energy;
    }
    
    public void setEnergy(final Integer Energy) {
        this.Energy = Energy;
    }
    
    public Integer getEnergyLastTime() {
        return this.EnergyLastTime;
    }
    
    public void setEnergyLastTime(final Integer EnergyLastTime) {
        this.EnergyLastTime = EnergyLastTime;
    }
    
    public String getEnergyBuyTime() {
        return this.EnergyBuyTime;
    }
    
    public void setEnergyBuyTime(final String EnergyBuyTime) {
        this.EnergyBuyTime = EnergyBuyTime;
    }
    
    public String getEnergyBuyCount() {
        return this.EnergyBuyCount;
    }
    
    public void setEnergyBuyCount(final String EnergyBuyCount) {
        this.EnergyBuyCount = EnergyBuyCount;
    }
    
    public Integer getEnergyMax() {
        return this.EnergyMax;
    }
    
    public void setEnergyMax(final Integer EnergyMax) {
        this.EnergyMax = EnergyMax;
    }
    
    public String getLeaderShip() {
        return this.LeaderShip;
    }
    
    public void setLeaderShip(final String LeaderShip) {
        this.LeaderShip = LeaderShip;
    }
    
    public String getFriendApplyNum() {
        return this.FriendApplyNum;
    }
    
    public void setFriendApplyNum(final String FriendApplyNum) {
        this.FriendApplyNum = FriendApplyNum;
    }
    
    public String getFriendNumMax() {
        return this.FriendNumMax;
    }
    
    public void setFriendNumMax(final String FriendNumMax) {
        this.FriendNumMax = FriendNumMax;
    }
    
    public Integer getDefaultGroupId() {
        return this.DefaultGroupId;
    }
    
    public void setDefaultGroupId(final Integer DefaultGroupId) {
        this.DefaultGroupId = DefaultGroupId;
    }
    
    public String getRankWin() {
        return this.RankWin;
    }
    
    public void setRankWin(final String RankWin) {
        this.RankWin = RankWin;
    }
    
    public String getRankLost() {
        return this.RankLost;
    }
    
    public void setRankLost(final String RankLost) {
        this.RankLost = RankLost;
    }
    
    public Integer getRankTimes() {
        return this.RankTimes;
    }
    
    public void setRankTimes(final Integer RankTimes) {
        this.RankTimes = RankTimes;
    }
    
    public String getThievesTimes() {
        return this.ThievesTimes;
    }
    
    public void setThievesTimes(final String ThievesTimes) {
        this.ThievesTimes = ThievesTimes;
    }
    
    public String getFragment5() {
        return this.Fragment5;
    }
    
    public void setFragment5(final String Fragment5) {
        this.Fragment5 = Fragment5;
    }
    
    public String getFragment4() {
        return this.Fragment4;
    }
    
    public void setFragment4(final String Fragment4) {
        this.Fragment4 = Fragment4;
    }
    
    public String getFragment3() {
        return this.Fragment3;
    }
    
    public void setFragment3(final String Fragment3) {
        this.Fragment3 = Fragment3;
    }
    
    public String getInviteCode() {
        return this.InviteCode;
    }
    
    public void setInviteCode(final String InviteCode) {
        this.InviteCode = InviteCode;
    }
    
    public String getInviteNum() {
        return this.InviteNum;
    }
    
    public void setInviteNum(final String InviteNum) {
        this.InviteNum = InviteNum;
    }
    
    public String getUdid() {
        return this.Udid;
    }
    
    public void setUdid(final String Udid) {
        this.Udid = Udid;
    }
    
    public String getLostPoint() {
        return this.LostPoint;
    }
    
    public void setLostPoint(final String LostPoint) {
        this.LostPoint = LostPoint;
    }
    
    public String getOrigin() {
        return this.Origin;
    }
    
    public void setOrigin(final String Origin) {
        this.Origin = Origin;
    }
    
    public Object getPlatform() {
        return this.Platform;
    }
    
    public void setPlatform(final Object Platform) {
        this.Platform = Platform;
    }
    
    public Object getLanguage() {
        return this.Language;
    }
    
    public void setLanguage(final Object Language) {
        this.Language = Language;
    }
    
    public String getAchievementPoint() {
        return this.AchievementPoint;
    }
    
    public void setAchievementPoint(final String AchievementPoint) {
        this.AchievementPoint = AchievementPoint;
    }
    
    public String getTotalDeposit() {
        return this.TotalDeposit;
    }
    
    public void setTotalDeposit(final String TotalDeposit) {
        this.TotalDeposit = TotalDeposit;
    }
    
    public Integer getLeaguePoint() {
        return this.LeaguePoint;
    }
    
    public void setLeaguePoint(final Integer LeaguePoint) {
        this.LeaguePoint = LeaguePoint;
    }
    
    public List<Integer> getCardMaterials() {
        return this.CardMaterials;
    }
    
    public void setCardMaterials(final List<Integer> CardMaterials) {
        this.CardMaterials = CardMaterials;
    }
    
    public String getUserName() {
        return this.UserName;
    }
    
    public void setUserName(final String UserName) {
        this.UserName = UserName;
    }
    
    public String getHP() {
        return this.HP;
    }
    
    public void setHP(final String HP) {
        this.HP = HP;
    }
    
    public String getPrevExp() {
        return this.PrevExp;
    }
    
    public void setPrevExp(final String PrevExp) {
        this.PrevExp = PrevExp;
    }
    
    public String getNextExp() {
        return this.NextExp;
    }
    
    public void setNextExp(final String NextExp) {
        this.NextExp = NextExp;
    }
    
    public Integer getVipLevel() {
        return this.VipLevel;
    }
    
    public void setVipLevel(final Integer VipLevel) {
        this.VipLevel = VipLevel;
    }
    
    public Integer getNewEmail() {
        return this.NewEmail;
    }
    
    public void setNewEmail(final Integer NewEmail) {
        this.NewEmail = NewEmail;
    }
    
    public Integer getSalaryCount() {
        return this.SalaryCount;
    }
    
    public void setSalaryCount(final Integer SalaryCount) {
        this.SalaryCount = SalaryCount;
    }
    
    public Integer getLoginContinueTimesXX() {
        return this.LoginContinueTimesXX;
    }
    
    public void setLoginContinueTimesXX(final Integer LoginContinueTimesXX) {
        this.LoginContinueTimesXX = LoginContinueTimesXX;
    }
    
    public Integer getMaxMapStageId() {
        return this.maxMapStageId;
    }
    
    public void setMaxMapStageId(final Integer maxMapStageId) {
        this.maxMapStageId = maxMapStageId;
    }
    
    public Integer getEvolutionTimes() {
        return this.EvolutionTimes;
    }
    
    public void setEvolutionTimes(final Integer EvolutionTimes) {
        this.EvolutionTimes = EvolutionTimes;
    }
    
    public Integer getTaskDailyReward() {
        return this.TaskDailyReward;
    }
    
    public void setTaskDailyReward(final Integer TaskDailyReward) {
        this.TaskDailyReward = TaskDailyReward;
    }
    
    public List<String> getLockedUserCardIds() {
        return this.LockedUserCardIds;
    }
    
    public void setLockedUserCardIds(final List<String> LockedUserCardIds) {
        this.LockedUserCardIds = LockedUserCardIds;
    }
    
    public Integer getPaid() {
        return this.Paid;
    }
    
    public void setPaid(final Integer Paid) {
        this.Paid = Paid;
    }
    
    public HydraActStatus getHydraActStatus() {
        return this.HydraActStatus;
    }
    
    public void setHydraActStatus(final HydraActStatus HydraActStatus) {
        this.HydraActStatus = HydraActStatus;
    }
    
    public Integer getTreeActStatus() {
        return this.TreeActStatus;
    }
    
    public void setTreeActStatus(final Integer TreeActStatus) {
        this.TreeActStatus = TreeActStatus;
    }
    
    public Integer getCountryWar() {
        return this.CountryWar;
    }
    
    public void setCountryWar(final Integer CountryWar) {
        this.CountryWar = CountryWar;
    }
    
    public Integer getElementWarActStatus() {
        return this.ElementWarActStatus;
    }
    
    public void setElementWarActStatus(final Integer ElementWarActStatus) {
        this.ElementWarActStatus = ElementWarActStatus;
    }
    
    public Integer getCardMakeStatus() {
        return this.CardMakeStatus;
    }
    
    public void setCardMakeStatus(final Integer CardMakeStatus) {
        this.CardMakeStatus = CardMakeStatus;
    }
    
    public Integer getBoss() {
        return this.Boss;
    }
    
    public void setBoss(final Integer Boss) {
        this.Boss = Boss;
    }
    
    public Integer getCanGetAchievement() {
        return this.CanGetAchievement;
    }
    
    public void setCanGetAchievement(final Integer CanGetAchievement) {
        this.CanGetAchievement = CanGetAchievement;
    }
    
    public Integer getRegtime() {
        return this.Regtime;
    }
    
    public void setRegtime(final Integer Regtime) {
        this.Regtime = Regtime;
    }
    
    public Integer getTimeWelfare() {
        return this.TimeWelfare;
    }
    
    public void setTimeWelfare(final Integer TimeWelfare) {
        this.TimeWelfare = TimeWelfare;
    }
    
    public Integer getBuyEnergyCost() {
        return this.BuyEnergyCost;
    }
    
    public void setBuyEnergyCost(final Integer BuyEnergyCost) {
        this.BuyEnergyCost = BuyEnergyCost;
    }
    
    public Integer getShowAD() {
        return this.showAD;
    }
    
    public void setShowAD(final Integer showAD) {
        this.showAD = showAD;
    }
    
    public Integer getShowEvolve() {
        return this.ShowEvolve;
    }
    
    public void setShowEvolve(final Integer ShowEvolve) {
        this.ShowEvolve = ShowEvolve;
    }
    
    public Integer getShowAcionView() {
        return this.showAcionView;
    }
    
    public void setShowAcionView(final Integer showAcionView) {
        this.showAcionView = showAcionView;
    }
    
    public Integer getShopFreeBuy() {
        return this.shopFreeBuy;
    }
    
    public void setShopFreeBuy(final Integer shopFreeBuy) {
        this.shopFreeBuy = shopFreeBuy;
    }
    
    public String getMaxCard() {
        return this.MaxCard;
    }
    
    public void setMaxCard(final String MaxCard) {
        this.MaxCard = MaxCard;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

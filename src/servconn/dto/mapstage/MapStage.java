// 
// EkUtils 
// 

package servconn.dto.mapstage;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class MapStage
{
    @Expose
    private String MapStageId;
    @Expose
    private String Name;
    @Expose
    private String Count;
    @Expose
    private String EverydayReward;
    @Expose
    private String Rank;
    @Expose
    private String MazeCount;
    @Expose
    private String NeedStar;
    @Expose
    private String Prev;
    @Expose
    private String Next;
    @Expose
    private List<MapStageDetail> MapStageDetails;
    
    public MapStage() {
        this.MapStageDetails = new ArrayList<MapStageDetail>();
    }
    
    public String getMapStageId() {
        return this.MapStageId;
    }
    
    public void setMapStageId(final String MapStageId) {
        this.MapStageId = MapStageId;
    }
    
    public String getName() {
        return this.Name;
    }
    
    public void setName(final String Name) {
        this.Name = Name;
    }
    
    public String getCount() {
        return this.Count;
    }
    
    public void setCount(final String Count) {
        this.Count = Count;
    }
    
    public String getEverydayReward() {
        return this.EverydayReward;
    }
    
    public void setEverydayReward(final String EverydayReward) {
        this.EverydayReward = EverydayReward;
    }
    
    public String getRank() {
        return this.Rank;
    }
    
    public void setRank(final String Rank) {
        this.Rank = Rank;
    }
    
    public String getMazeCount() {
        return this.MazeCount;
    }
    
    public void setMazeCount(final String MazeCount) {
        this.MazeCount = MazeCount;
    }
    
    public String getNeedStar() {
        return this.NeedStar;
    }
    
    public void setNeedStar(final String NeedStar) {
        this.NeedStar = NeedStar;
    }
    
    public String getPrev() {
        return this.Prev;
    }
    
    public void setPrev(final String Prev) {
        this.Prev = Prev;
    }
    
    public String getNext() {
        return this.Next;
    }
    
    public void setNext(final String Next) {
        this.Next = Next;
    }
    
    public List<MapStageDetail> getMapStageDetails() {
        return this.MapStageDetails;
    }
    
    public void setMapStageDetails(final List<MapStageDetail> MapStageDetails) {
        this.MapStageDetails = MapStageDetails;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

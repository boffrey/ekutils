// 
// EkUtils 
// 

package servconn.dto.mapstage;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class MapStageDetail
{
    @Expose
    private String MapStageDetailId;
    @Expose
    private String Name;
    @Expose
    private String Type;
    @Expose
    private String MapStageId;
    @Expose
    private String Rank;
    @Expose
    private String X;
    @Expose
    private String Y;
    @Expose
    private String Prev;
    @Expose
    private String Next;
    @Expose
    private String NextBranch;
    @Expose
    private String FightName;
    @Expose
    private String FightImg;
    @Expose
    private Object Dialogue;
    @Expose
    private Object DialogueAfter;
    @Expose
    private List<Level> Levels;
    
    public MapStageDetail() {
        this.Levels = new ArrayList<Level>();
    }
    
    public String getMapStageDetailId() {
        return this.MapStageDetailId;
    }
    
    public void setMapStageDetailId(final String MapStageDetailId) {
        this.MapStageDetailId = MapStageDetailId;
    }
    
    public String getName() {
        return this.Name;
    }
    
    public void setName(final String Name) {
        this.Name = Name;
    }
    
    public String getType() {
        return this.Type;
    }
    
    public void setType(final String Type) {
        this.Type = Type;
    }
    
    public String getMapStageId() {
        return this.MapStageId;
    }
    
    public void setMapStageId(final String MapStageId) {
        this.MapStageId = MapStageId;
    }
    
    public String getRank() {
        return this.Rank;
    }
    
    public void setRank(final String Rank) {
        this.Rank = Rank;
    }
    
    public String getX() {
        return this.X;
    }
    
    public void setX(final String X) {
        this.X = X;
    }
    
    public String getY() {
        return this.Y;
    }
    
    public void setY(final String Y) {
        this.Y = Y;
    }
    
    public String getPrev() {
        return this.Prev;
    }
    
    public void setPrev(final String Prev) {
        this.Prev = Prev;
    }
    
    public String getNext() {
        return this.Next;
    }
    
    public void setNext(final String Next) {
        this.Next = Next;
    }
    
    public String getNextBranch() {
        return this.NextBranch;
    }
    
    public void setNextBranch(final String NextBranch) {
        this.NextBranch = NextBranch;
    }
    
    public String getFightName() {
        return this.FightName;
    }
    
    public void setFightName(final String FightName) {
        this.FightName = FightName;
    }
    
    public String getFightImg() {
        return this.FightImg;
    }
    
    public void setFightImg(final String FightImg) {
        this.FightImg = FightImg;
    }
    
    public Object getDialogue() {
        return this.Dialogue;
    }
    
    public void setDialogue(final Object Dialogue) {
        this.Dialogue = Dialogue;
    }
    
    public Object getDialogueAfter() {
        return this.DialogueAfter;
    }
    
    public void setDialogueAfter(final Object DialogueAfter) {
        this.DialogueAfter = DialogueAfter;
    }
    
    public List<Level> getLevels() {
        return this.Levels;
    }
    
    public void setLevels(final List<Level> levels) {
        this.Levels = levels;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

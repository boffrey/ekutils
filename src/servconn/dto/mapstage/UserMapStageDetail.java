// 
// EkUtils 
// 

package servconn.dto.mapstage;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserMapStageDetail
{
    @SerializedName("Uid")
    @Expose
    private String Uid;
    @SerializedName("MapStageDetailId")
    @Expose
    private String MapStageDetailId;
    @SerializedName("Type")
    @Expose
    private String Type;
    @SerializedName("MapStageId")
    @Expose
    private String MapStageId;
    @SerializedName("FinishedStage")
    @Expose
    private String FinishedStage;
    @SerializedName("LastFinishedTime")
    @Expose
    private String LastFinishedTime;
    @SerializedName("CounterAttackTime")
    @Expose
    private String CounterAttackTime;
    
    public String getUid() {
        return this.Uid;
    }
    
    public void setUid(final String uid) {
        this.Uid = uid;
    }
    
    public String getMapStageDetailId() {
        return this.MapStageDetailId;
    }
    
    public void setMapStageDetailId(final String mapStageDetailId) {
        this.MapStageDetailId = mapStageDetailId;
    }
    
    public String getType() {
        return this.Type;
    }
    
    public void setType(final String type) {
        this.Type = type;
    }
    
    public String getMapStageId() {
        return this.MapStageId;
    }
    
    public void setMapStageId(final String mapStageId) {
        this.MapStageId = mapStageId;
    }
    
    public String getFinishedStage() {
        return this.FinishedStage;
    }
    
    public void setFinishedStage(final String finishedStage) {
        this.FinishedStage = finishedStage;
    }
    
    public String getLastFinishedTime() {
        return this.LastFinishedTime;
    }
    
    public void setLastFinishedTime(final String lastFinishedTime) {
        this.LastFinishedTime = lastFinishedTime;
    }
    
    public String getCounterAttackTime() {
        return this.CounterAttackTime;
    }
    
    public void setCounterAttackTime(final String counterAttackTime) {
        this.CounterAttackTime = counterAttackTime;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

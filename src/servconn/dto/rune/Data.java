// 
// EkUtils 
// 

package servconn.dto.rune;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class Data
{
    @Expose
    private List<Rune> Runes;
    
    public Data() {
        this.Runes = new ArrayList<Rune>();
    }
    
    public List<Rune> getRunes() {
        return this.Runes;
    }
    
    public void setRunes(final List<Rune> Runes) {
        this.Runes = Runes;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

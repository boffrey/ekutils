// 
// EkUtils 
// 

package servconn.dto.rune;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class Rune
{
    @Expose
    private String RuneId;
    @Expose
    private String RuneName;
    @Expose
    private String Property;
    @Expose
    private String Color;
    @Expose
    private String LockSkill1;
    @Expose
    private String LockSkill2;
    @Expose
    private String LockSkill3;
    @Expose
    private String LockSkill4;
    @Expose
    private String LockSkill5;
    @Expose
    private String Price;
    @Expose
    private String SkillTimes;
    @Expose
    private String Condition;
    @Expose
    private String SkillConditionSlide;
    @Expose
    private String SkillConditionType;
    @Expose
    private String SkillConditionRace;
    @Expose
    private String SkillConditionColor;
    @Expose
    private String SkillConditionCompare;
    @Expose
    private String SkillConditionValue;
    @Expose
    private String ThinkGet;
    @Expose
    private String Fragment;
    @Expose
    private String BaseExp;
    @Expose
    private List<String> ExpArray;
    
    public Rune() {
        this.ExpArray = new ArrayList<String>();
    }
    
    public String getRuneId() {
        return this.RuneId;
    }
    
    public void setRuneId(final String RuneId) {
        this.RuneId = RuneId;
    }
    
    public String getRuneName() {
        return this.RuneName;
    }
    
    public void setRuneName(final String RuneName) {
        this.RuneName = RuneName;
    }
    
    public String getProperty() {
        return this.Property;
    }
    
    public void setProperty(final String Property) {
        this.Property = Property;
    }
    
    public String getColor() {
        return this.Color;
    }
    
    public void setColor(final String Color) {
        this.Color = Color;
    }
    
    public String getLockSkill1() {
        return this.LockSkill1;
    }
    
    public void setLockSkill1(final String LockSkill1) {
        this.LockSkill1 = LockSkill1;
    }
    
    public String getLockSkill2() {
        return this.LockSkill2;
    }
    
    public void setLockSkill2(final String LockSkill2) {
        this.LockSkill2 = LockSkill2;
    }
    
    public String getLockSkill3() {
        return this.LockSkill3;
    }
    
    public void setLockSkill3(final String LockSkill3) {
        this.LockSkill3 = LockSkill3;
    }
    
    public String getLockSkill4() {
        return this.LockSkill4;
    }
    
    public void setLockSkill4(final String LockSkill4) {
        this.LockSkill4 = LockSkill4;
    }
    
    public String getLockSkill5() {
        return this.LockSkill5;
    }
    
    public void setLockSkill5(final String LockSkill5) {
        this.LockSkill5 = LockSkill5;
    }
    
    public String getPrice() {
        return this.Price;
    }
    
    public void setPrice(final String Price) {
        this.Price = Price;
    }
    
    public String getSkillTimes() {
        return this.SkillTimes;
    }
    
    public void setSkillTimes(final String SkillTimes) {
        this.SkillTimes = SkillTimes;
    }
    
    public String getCondition() {
        return this.Condition;
    }
    
    public void setCondition(final String Condition) {
        this.Condition = Condition;
    }
    
    public String getSkillConditionSlide() {
        return this.SkillConditionSlide;
    }
    
    public void setSkillConditionSlide(final String SkillConditionSlide) {
        this.SkillConditionSlide = SkillConditionSlide;
    }
    
    public String getSkillConditionType() {
        return this.SkillConditionType;
    }
    
    public void setSkillConditionType(final String SkillConditionType) {
        this.SkillConditionType = SkillConditionType;
    }
    
    public String getSkillConditionRace() {
        return this.SkillConditionRace;
    }
    
    public void setSkillConditionRace(final String SkillConditionRace) {
        this.SkillConditionRace = SkillConditionRace;
    }
    
    public String getSkillConditionColor() {
        return this.SkillConditionColor;
    }
    
    public void setSkillConditionColor(final String SkillConditionColor) {
        this.SkillConditionColor = SkillConditionColor;
    }
    
    public String getSkillConditionCompare() {
        return this.SkillConditionCompare;
    }
    
    public void setSkillConditionCompare(final String SkillConditionCompare) {
        this.SkillConditionCompare = SkillConditionCompare;
    }
    
    public String getSkillConditionValue() {
        return this.SkillConditionValue;
    }
    
    public void setSkillConditionValue(final String SkillConditionValue) {
        this.SkillConditionValue = SkillConditionValue;
    }
    
    public String getThinkGet() {
        return this.ThinkGet;
    }
    
    public void setThinkGet(final String ThinkGet) {
        this.ThinkGet = ThinkGet;
    }
    
    public String getFragment() {
        return this.Fragment;
    }
    
    public void setFragment(final String Fragment) {
        this.Fragment = Fragment;
    }
    
    public String getBaseExp() {
        return this.BaseExp;
    }
    
    public void setBaseExp(final String BaseExp) {
        this.BaseExp = BaseExp;
    }
    
    public List<String> getExpArray() {
        return this.ExpArray;
    }
    
    public void setExpArray(final List<String> ExpArray) {
        this.ExpArray = ExpArray;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

// 
// EkUtils 
// 

package servconn.dto.legion;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;

public class MyLegion
{
    @Expose
    private String LegionId;
    @Expose
    private String Name;
    @Expose
    private Integer HeadId;
    @Expose
    private Integer Emblem;
    @Expose
    private String Desc;
    @Expose
    private String Notice;
    @Expose
    private Integer Force;
    @Expose
    private String Attr;
    @Expose
    private String Slogan;
    @Expose
    private String Contribute9;
    @Expose
    private String Members;
    @Expose
    private String CreateTime;
    @Expose
    private String CreateId;
    @Expose
    private String EmblemLevel;
    @Expose
    private String Contribute1;
    @Expose
    private String Contribute2;
    @Expose
    private String Contribute3;
    @Expose
    private String Contribute4;
    @Expose
    private String Contribute5;
    @Expose
    private String Contribute6;
    @Expose
    private String Contribute7;
    @Expose
    private String Contribute8;
    @Expose
    private Integer Resources;
    @Expose
    private Integer Rank;
    @Expose
    private String AddCountryWarBtnShow;
    @Expose
    private String ApplyLeader;
    @SerializedName("join_legion_fight")
    @Expose
    private Integer joinLegionFight;
    @Expose
    private String MemberCount;
    @Expose
    private Integer NextLevelResoures;
    @Expose
    private List<LegionAward> Award;
    @Expose
    private Integer LegionLevel;
    @Expose
    private String HeadName;
    @Expose
    private String HeadLevel;
    @Expose
    private String HeadSex;
    
    public MyLegion() {
        this.Award = new ArrayList<LegionAward>();
    }
    
    public String getLegionId() {
        return this.LegionId;
    }
    
    public void setLegionId(final String LegionId) {
        this.LegionId = LegionId;
    }
    
    public String getName() {
        return this.Name;
    }
    
    public void setName(final String Name) {
        this.Name = Name;
    }
    
    public Integer getHeadId() {
        return this.HeadId;
    }
    
    public void setHeadId(final Integer HeadId) {
        this.HeadId = HeadId;
    }
    
    public Integer getEmblem() {
        return this.Emblem;
    }
    
    public void setEmblem(final Integer Emblem) {
        this.Emblem = Emblem;
    }
    
    public String getDesc() {
        return this.Desc;
    }
    
    public void setDesc(final String Desc) {
        this.Desc = Desc;
    }
    
    public String getNotice() {
        return this.Notice;
    }
    
    public void setNotice(final String Notice) {
        this.Notice = Notice;
    }
    
    public Integer getForce() {
        return this.Force;
    }
    
    public void setForce(final Integer Force) {
        this.Force = Force;
    }
    
    public String getAttr() {
        return this.Attr;
    }
    
    public void setAttr(final String Attr) {
        this.Attr = Attr;
    }
    
    public String getSlogan() {
        return this.Slogan;
    }
    
    public void setSlogan(final String Slogan) {
        this.Slogan = Slogan;
    }
    
    public String getContribute9() {
        return this.Contribute9;
    }
    
    public void setContribute9(final String Contribute9) {
        this.Contribute9 = Contribute9;
    }
    
    public String getMembers() {
        return this.Members;
    }
    
    public void setMembers(final String Members) {
        this.Members = Members;
    }
    
    public String getCreateTime() {
        return this.CreateTime;
    }
    
    public void setCreateTime(final String CreateTime) {
        this.CreateTime = CreateTime;
    }
    
    public String getCreateId() {
        return this.CreateId;
    }
    
    public void setCreateId(final String CreateId) {
        this.CreateId = CreateId;
    }
    
    public String getEmblemLevel() {
        return this.EmblemLevel;
    }
    
    public void setEmblemLevel(final String EmblemLevel) {
        this.EmblemLevel = EmblemLevel;
    }
    
    public String getContribute1() {
        return this.Contribute1;
    }
    
    public void setContribute1(final String Contribute1) {
        this.Contribute1 = Contribute1;
    }
    
    public String getContribute2() {
        return this.Contribute2;
    }
    
    public void setContribute2(final String Contribute2) {
        this.Contribute2 = Contribute2;
    }
    
    public String getContribute3() {
        return this.Contribute3;
    }
    
    public void setContribute3(final String Contribute3) {
        this.Contribute3 = Contribute3;
    }
    
    public String getContribute4() {
        return this.Contribute4;
    }
    
    public void setContribute4(final String Contribute4) {
        this.Contribute4 = Contribute4;
    }
    
    public String getContribute5() {
        return this.Contribute5;
    }
    
    public void setContribute5(final String Contribute5) {
        this.Contribute5 = Contribute5;
    }
    
    public String getContribute6() {
        return this.Contribute6;
    }
    
    public void setContribute6(final String Contribute6) {
        this.Contribute6 = Contribute6;
    }
    
    public String getContribute7() {
        return this.Contribute7;
    }
    
    public void setContribute7(final String Contribute7) {
        this.Contribute7 = Contribute7;
    }
    
    public String getContribute8() {
        return this.Contribute8;
    }
    
    public void setContribute8(final String Contribute8) {
        this.Contribute8 = Contribute8;
    }
    
    public Integer getResources() {
        return this.Resources;
    }
    
    public void setResources(final Integer Resources) {
        this.Resources = Resources;
    }
    
    public Integer getRank() {
        return this.Rank;
    }
    
    public void setRank(final Integer Rank) {
        this.Rank = Rank;
    }
    
    public String getAddCountryWarBtnShow() {
        return this.AddCountryWarBtnShow;
    }
    
    public void setAddCountryWarBtnShow(final String AddCountryWarBtnShow) {
        this.AddCountryWarBtnShow = AddCountryWarBtnShow;
    }
    
    public String getApplyLeader() {
        return this.ApplyLeader;
    }
    
    public void setApplyLeader(final String ApplyLeader) {
        this.ApplyLeader = ApplyLeader;
    }
    
    public Integer getJoinLegionFight() {
        return this.joinLegionFight;
    }
    
    public void setJoinLegionFight(final Integer joinLegionFight) {
        this.joinLegionFight = joinLegionFight;
    }
    
    public String getMemberCount() {
        return this.MemberCount;
    }
    
    public void setMemberCount(final String MemberCount) {
        this.MemberCount = MemberCount;
    }
    
    public Integer getNextLevelResoures() {
        return this.NextLevelResoures;
    }
    
    public void setNextLevelResoures(final Integer NextLevelResoures) {
        this.NextLevelResoures = NextLevelResoures;
    }
    
    public List<LegionAward> getAward() {
        return this.Award;
    }
    
    public void setAward(final List<LegionAward> Award) {
        this.Award = Award;
    }
    
    public Integer getLegionLevel() {
        return this.LegionLevel;
    }
    
    public void setLegionLevel(final Integer LegionLevel) {
        this.LegionLevel = LegionLevel;
    }
    
    public String getHeadName() {
        return this.HeadName;
    }
    
    public void setHeadName(final String HeadName) {
        this.HeadName = HeadName;
    }
    
    public String getHeadLevel() {
        return this.HeadLevel;
    }
    
    public void setHeadLevel(final String HeadLevel) {
        this.HeadLevel = HeadLevel;
    }
    
    public String getHeadSex() {
        return this.HeadSex;
    }
    
    public void setHeadSex(final String HeadSex) {
        this.HeadSex = HeadSex;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

// 
// EkUtils 
// 

package servconn.dto.legion;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class LegionListData
{
    @Expose
    private MyLegion MyLegion;
    @Expose
    private MyInfo MyInfo;
    @Expose
    private List<LegionInfo> LegionInfos;
    @Expose
    private String Count;
    
    public LegionListData() {
        this.LegionInfos = new ArrayList<LegionInfo>();
    }
    
    public MyLegion getMyLegion() {
        return this.MyLegion;
    }
    
    public void setMyLegion(final MyLegion MyLegion) {
        this.MyLegion = MyLegion;
    }
    
    public MyInfo getMyInfo() {
        return this.MyInfo;
    }
    
    public void setMyInfo(final MyInfo MyInfo) {
        this.MyInfo = MyInfo;
    }
    
    public List<LegionInfo> getLegionInfos() {
        return this.LegionInfos;
    }
    
    public void setLegionInfos(final List<LegionInfo> LegionInfos) {
        this.LegionInfos = LegionInfos;
    }
    
    public String getCount() {
        return this.Count;
    }
    
    public void setCount(final String Count) {
        this.Count = Count;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

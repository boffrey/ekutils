// 
// EkUtils 
// 

package servconn.dto.legion;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class MyInfo
{
    @Expose
    private Integer Uid;
    @Expose
    private Integer LegionId;
    @Expose
    private String JoinTime;
    @Expose
    private Integer Contribute;
    @Expose
    private Integer Duty;
    @Expose
    private Integer Honor;
    
    public Integer getUid() {
        return this.Uid;
    }
    
    public void setUid(final Integer Uid) {
        this.Uid = Uid;
    }
    
    public Integer getLegionId() {
        return this.LegionId;
    }
    
    public void setLegionId(final Integer LegionId) {
        this.LegionId = LegionId;
    }
    
    public String getJoinTime() {
        return this.JoinTime;
    }
    
    public void setJoinTime(final String JoinTime) {
        this.JoinTime = JoinTime;
    }
    
    public Integer getContribute() {
        return this.Contribute;
    }
    
    public void setContribute(final Integer Contribute) {
        this.Contribute = Contribute;
    }
    
    public Integer getDuty() {
        return this.Duty;
    }
    
    public void setDuty(final Integer Duty) {
        this.Duty = Duty;
    }
    
    public Integer getHonor() {
        return this.Honor;
    }
    
    public void setHonor(final Integer Honor) {
        this.Honor = Honor;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

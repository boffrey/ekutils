// 
// EkUtils 
// 

package servconn.dto.usercard;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class Data
{
    @Expose
    private List<UserCard> Cards;
    
    public Data() {
        this.Cards = new ArrayList<UserCard>();
    }
    
    public List<UserCard> getCards() {
        return this.Cards;
    }
    
    public void setCards(final List<UserCard> Cards) {
        this.Cards = Cards;
    }
}

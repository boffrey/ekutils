// 
// EkUtils 
// 

package servconn.dto.arena;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class Data
{
    @Expose
    private List<Competitor> Competitors;
    @Expose
    private Integer Rank;
    
    public Data() {
        this.Competitors = new ArrayList<Competitor>();
    }
    
    public List<Competitor> getCompetitors() {
        return this.Competitors;
    }
    
    public void setCompetitors(final List<Competitor> Competitors) {
        this.Competitors = Competitors;
    }
    
    public Integer getRank() {
        return this.Rank;
    }
    
    public void setRank(final Integer Rank) {
        this.Rank = Rank;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}

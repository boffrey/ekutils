// 
// EkUtils 
// 

package ekutils;

import java.io.IOException;
import java.util.Map;
import servconn.dto.card.Card;
import servconn.client.EkClient;

public class CardDbAnalyzer
{
    public void getServerKWPool(final String server) throws IOException {
        final EkClient client = new EkClient();
        final Map<String, Card> cardMap = client.getServerCards(server);
        final String format = "%1$-20s|%2$-6s\n";
        System.out.println("KW Pool");
        for (final Card card : cardMap.values()) {
            if (!card.getForceFightExchange().equals("0")) {
                System.out.format(format, card.getCardName(), card.getForceExchangeInitPrice());
            }
        }
    }
}

package ekutils;

import java.util.Collections;
import servconn.sim.MitziSimFoHPrinter;
import java.util.ArrayList;
import servconn.sim.FoHPrinter;
import servconn.dto.league.LeagueData;
import servconn.dto.rune.Rune;
import servconn.dto.skill.Skill;
import servconn.dto.card.Card;
import java.util.Map;
import java.io.PrintWriter;
import servconn.sim.FoHPrinterFactory;
import servconn.client.EkClient;
import java.io.IOException;
import java.util.List;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.config.Configuration;
import java.net.UnknownHostException;
import ekutils.options.BootException;
import org.apache.commons.lang3.StringUtils;
import ekutils.dto.MazePlayMode;
import servconn.client.EkUserClient;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import ekutils.options.BootOptions;
import org.apache.logging.log4j.Logger;

public class Application
{
    static final Logger LOGGER;
    
    public static void main(final String[] args) throws IOException, InterruptedException {
        try {
            final BootOptions bootOptions = new BootOptions(args);
            if (bootOptions.isDebug()) {
                final LoggerContext ctx = (LoggerContext)LogManager.getContext(false);
                final Configuration config = ctx.getConfiguration();
                final LoggerConfig loggerConfig = config.getLoggerConfig("");
                loggerConfig.setLevel(Level.DEBUG);
                ctx.updateLoggers();
            }
            String propertyFile = UtilCons.PROPERTYFILE;
            if (bootOptions.isPropertyFile()) {
                propertyFile = bootOptions.getPropertyFile();
            }
            Config.initialize(propertyFile);
            boolean missingFiles = false;
            final boolean downloadCards = bootOptions.isDownloadResources() && bootOptions.getDownloadResources().contains("cards");
            final boolean downloadSkills = bootOptions.isDownloadResources() && bootOptions.getDownloadResources().contains("skills");
            final boolean downloadRunes = bootOptions.isDownloadResources() && bootOptions.getDownloadResources().contains("runes");
            if (!Config.fileExists(UtilCons.CARDSFILE) && !downloadCards) {
                Application.LOGGER.error("Missing " + UtilCons.CARDSFILE + ", download using -dr cards");
                missingFiles = true;
            }
            else if (downloadCards) {
                EkUtils.saveServerCardsToFile(Config.getProperty("defaultServer"), UtilCons.CARDSFILE);
            }
            if (!Config.fileExists(UtilCons.RUNESFILE) && !downloadRunes) {
                Application.LOGGER.error("Missing " + UtilCons.RUNESFILE + ", download using -dr runes");
                missingFiles = true;
            }
            else if (downloadRunes) {
                EkUtils.saveServerRunesToFile(Config.getProperty("defaultServer"), UtilCons.RUNESFILE);
            }
            if (!Config.fileExists(UtilCons.SKILLSFILE) && !downloadSkills) {
                Application.LOGGER.error("Missing " + UtilCons.SKILLSFILE + ", download using -dr skills");
                missingFiles = true;
            }
            else if (downloadSkills) {
                EkUtils.saveServerSkillsToFile(Config.getProperty("defaultServer"), UtilCons.SKILLSFILE);
            }
            if (missingFiles) {
                System.exit(-1);
            }
            else if (!Config.fileExists(UtilCons.CARDSFILE) || !Config.fileExists(UtilCons.RUNESFILE) || !Config.fileExists(UtilCons.SKILLSFILE)) {
                Application.LOGGER.fatal("Something went terribly wrong, try to redownload json files from the server");
                System.exit(-1);
            }
            final EkUserClient client = new EkUserClient(Config.getProperty("email"), Config.getProperty("password"));
            if (bootOptions.isPlayMaze()) {
                final int mapStageId = bootOptions.getMazeMapStage();
                if (mapStageId == -1) {
                    Application.LOGGER.error("Invalid maze map");
                    System.exit(-1);
                }
                final MazeUtils mu = new MazeUtils(client);
                if (bootOptions.isMazeClearChests()) {
                    mu.playMaze(mapStageId, MazePlayMode.CHESTS);
                }
                else if (bootOptions.isMazeClearStairs()) {
                    mu.playMaze(mapStageId, MazePlayMode.STAIRS);
                }
                else if (bootOptions.isMazeLeaveOne()) {
                    mu.playMaze(mapStageId, MazePlayMode.LEAVEONE);
                }
                else if (bootOptions.isMazeClearMonsters()) {
                    mu.playMaze(mapStageId, MazePlayMode.MONSTERS);
                }
                else if (bootOptions.isMazeClearFull()) {
                    mu.playMaze(mapStageId, MazePlayMode.FINISH);
                }
            }
            else {
                if (bootOptions.isPrintDecks()) {
                    if (bootOptions.isPrintDecksForEku()) {
                        final DeckUtils du = new DeckUtils(client, "eku");
                        du.setConsole(false);
                        du.setOutputPath(bootOptions.getOutputPath());
                        du.printDecks();
                    }
                    else {
                        final DeckUtils du = new DeckUtils(client, "crys");
                        du.setConsole(false);
                        du.setOutputPath(bootOptions.getOutputPath());
                        du.printDecks();
                    }
                    return;
                }
                if (bootOptions.isPrintCards()) {
                    final UserCardUtils ucu = new UserCardUtils(client);
                    final List<String> cardTypes = bootOptions.getCardTypesForPrinting();
                    ucu.setCardTypes(cardTypes);
                    ucu.printUserCards();
                    if (cardTypes.contains("frags")) {
                        ucu.printCardFragments();
                    }
                }
                else if (bootOptions.isFilterCards()) {
                    final List<String> cardFilter = bootOptions.getCardFilters();
                    if (cardFilter.contains("kwpool")) {
                        EkUtils.displayKWPool();
                        return;
                    }
                    if (cardFilter.contains("thiefdrops")) {
                        EkUtils.displayThiefDrops();
                        return;
                    }
                }
                else if (bootOptions.isBuyGoods()) {
                    final List<String> optValuesList = bootOptions.getBuyingGoodArguments();
                    final BuyingUtils bu = new BuyingUtils(client);
                    bu.buyGood(optValuesList.get(0), optValuesList.get(1));
                }
                else if (bootOptions.isClaimGiftCode()) {
                    final String claimGiftCodeArgument = bootOptions.getClaimGiftCodeArgument();
                    final ClaimUtils cu = new ClaimUtils(client);
                    cu.setArgument(claimGiftCodeArgument);
                    cu.claimGiftCode();
                }
                else if (bootOptions.isHydraStats()) {
                    final HydraUtils hu = new HydraUtils(client);
                    final Integer totalContributions = hu.getClanContibutions();
                    final Integer unclaimedPoints = hu.printUnclaimedUserHydras();
                    hu.getHydraRankingSelf(totalContributions, unclaimedPoints);
                    final UserCardUtils ucu2 = new UserCardUtils(client);
                    ucu2.printCollectionCards();
                }
                else if (bootOptions.isClaimHydra()) {
                    final List<String> hydraLevels = bootOptions.getHydraLevelsForClaiming();
                    if (hydraLevels.size() > 0) {
                        final HydraUtils hu2 = new HydraUtils(client);
                        hu2.claimUserHydras(hydraLevels);
                    }
                }
                else if (bootOptions.isPrintEWDeck()) {
                    final String deckString = bootOptions.getPrintEWDeckArgument();
                    final DeckUtils du2 = new DeckUtils(client, "crys");
                    du2.printEWDeck(deckString);
                }
                else if (bootOptions.isMeditate()) {
                    final int numOfMeditations = bootOptions.getMeditateArgument();
                    if (numOfMeditations <= 0) {
                        Application.LOGGER.error("Invalid argument for number of meditations, must be a positive integer");
                        System.exit(-1);
                    }
                    final MeditateUtils mu2 = new MeditateUtils(client);
                    mu2.meditate(numOfMeditations);
                }
                else if (bootOptions.isStatus()) {
                    final UserFunctions uf = new UserFunctions(client);
                    uf.userStatus();
                }
                else if (bootOptions.isPrintEwBossDecks()) {
                    final ElementalWarUtils ewu = new ElementalWarUtils(client);
                    ewu.printElementalWarDecks();
                }
                else if (bootOptions.isSellCards()) {
                    final int numOfCards = bootOptions.getSellCardLimit();
                    if (numOfCards <= 0) {
                        Application.LOGGER.error("Invalid argument for number of cards to sell, must be a positive integer or 'all'");
                        System.exit(-1);
                    }
                    final SellingUtils su = new SellingUtils(client);
                    su.setSellLimit(bootOptions.getSellCardLimit());
                    su.setSellArgument(bootOptions.getSellCardArgument());
                    su.sellCards();
                }
                else if (bootOptions.isMazeStatus()) {
                    final MazeUtils mu3 = new MazeUtils(client);
                    mu3.printMazeStatus();
                }
                else if (bootOptions.isFohInfo()) {
                    final FohUtils mu3 = new FohUtils(client);
                    mu3.printFohInfo();
                }
                else if(bootOptions.isPrintFriends()) {
                	final FriendUtils fu = new FriendUtils(client);
                	fu.GetFriendsList();
                }
            }
        }
        catch (BootException e) {
            if (StringUtils.isEmpty(e.getMessage())) {
                BootOptions.printHelp(BootOptions.HELP_HEADER);
                System.exit(0);
            }
            else {
                Application.LOGGER.error(e.getMessage());
                System.exit(-1);
            }
        }
        catch (IllegalArgumentException ie) {
            Application.LOGGER.error(ie.getMessage());
            System.exit(-1);
        }
        catch (IllegalStateException ie2) {
            Application.LOGGER.error(ie2.getMessage());
            System.exit(-1);
        }
        catch (UnknownHostException uhe) {
            Application.LOGGER.error("Check your internet connection");
            Application.LOGGER.debug(uhe);
            System.exit(-1);
        }
        catch (Exception e2) {
            Application.LOGGER.error(e2);
            e2.printStackTrace();
            System.exit(-1);
        }
        Application.LOGGER.debug("Default server " + Config.getProperty("defaultServer"));
        final UtilCons.Mode mode = UtilCons.Mode.NONE;
        switch (mode) {
            case PRINTDECKS: {
                printDecksForServer(Config.getProperty("defaultServer"), "E:\\cards.json");
                break;
            }
            case PRINTARENADECKS: {
                printArenaDecks(Config.getProperty("email"), Config.getProperty("password"));
                break;
            }
            case NOTIMPLEMENTEDSKILLS: {
                displayNotImplementedSkills(Config.getProperty("defaultServer"));
                break;
            }
            case SAVEFILES: {
                EkUtils.saveServerRunesToFile(Config.getProperty("defaultServer"), "E:\\runes.json");
                break;
            }
			default:
				break;
        }
    }
    
    public static void printDecksForServer(final String server, final String filename) throws IOException {
        final EkClient client = new EkClient();
        final Map<String, Card> cardMap = client.getServerCards(server);
        final Map<String, Skill> skillMap = client.getServerSkills(server);
        final Map<String, Rune> runeMap = client.getServerRunes(server);
        final LeagueData leagueData = client.getLeagueData(server);
        final FoHPrinter fohPrinter = FoHPrinterFactory.getFoHPrinter("MITZI");
        fohPrinter.setGameInfo(cardMap, skillMap, runeMap);
        fohPrinter.setEvoNames(UtilCons.evoNames);
        final String decks = fohPrinter.printDeck(leagueData);
        System.out.println(decks);
        final PrintWriter writer = new PrintWriter(filename, "UTF-8");
        writer.println(decks);
        writer.close();
    }
    
    public static void printArenaDecks(final String email, final String password) throws IOException, InterruptedException {
        final ArenaDeckUtils adu = new ArenaDeckUtils(email, password, Config.getProperty("defaultServer"));
        adu.setEvoNames(UtilCons.evoNames);
        adu.setStart(1);
        adu.setAmount(10);
        final String decks = adu.printArenaDecks();
        System.out.println(decks);
    }
    
    public static void displayNotImplementedSkills(final String server) throws IOException {
        final EkClient client = new EkClient();
        final List<String> skillList = new ArrayList<String>();
        final Map<String, Skill> skills = client.getServerSkills(server);
        final FoHPrinter fohPrinter = FoHPrinterFactory.getFoHPrinter("MITZI");
        fohPrinter.setEvoNames(UtilCons.evoNames);
        for (final Skill skill : skills.values()) {
            final String convertedSkill = ((MitziSimFoHPrinter)fohPrinter).convertSkillName(skill.getName());
            if ("".equals(convertedSkill)) {
                skillList.add(skill.getName());
            }
        }
        Collections.sort(skillList, String.CASE_INSENSITIVE_ORDER);
        for (final String skill2 : skillList) {
            System.out.println(skill2);
        }
    }
    
    static {
        LOGGER = LogManager.getLogger();
    }
}

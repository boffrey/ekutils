// 
// EkUtils 
// 

package ekutils;

import com.google.gson.JsonParser;
import com.google.gson.GsonBuilder;
import servconn.client.EkClient;
import servconn.dto.league.Card;
import java.util.ArrayList;
import java.io.IOException;
import servconn.dto.common.Deck;
import java.util.Iterator;
import java.util.List;
import servconn.dto.mapstage.MapStageData;
import servconn.dto.mapstage.Level;
import servconn.dto.mapstage.MapStageDetail;
import servconn.dto.mapstage.MapStage;
import ekdao.dao.DaoFactory;
import ekutils.printer.CrystarkRunePrinter;
import ekutils.printer.CrystarkCardPrinter;
import ekdao.dao.RuneDao;
import ekdao.dao.SkillDao;
import ekdao.dao.CardDao;
import servconn.client.EkUserClient;

public class MapUtils
{
    private EkUserClient client;
    private CardDao cardDao;
    private SkillDao skillDao;
    private RuneDao runeDao;
    private CrystarkCardPrinter cardPrinter;
    private CrystarkRunePrinter runePrinter;
    private static String cardList;
    
    public MapUtils(final EkUserClient client) {
        this.client = client;
        this.cardDao = DaoFactory.getCardDao(UtilCons.CARDSFILE);
        this.skillDao = DaoFactory.getSkillDao(UtilCons.SKILLSFILE);
        this.runeDao = DaoFactory.getRuneDao(UtilCons.RUNESFILE);
        (this.cardPrinter = new CrystarkCardPrinter()).setCardDao(this.cardDao);
        this.cardPrinter.setSkillDao(this.skillDao);
        (this.runePrinter = new CrystarkRunePrinter()).setRuneDao(this.runeDao);
    }
    
    public void printMapDecks() throws IOException {
        final MapStageData mapStageData = this.client.getServerMapStages();
        final List<MapStage> mapStages = mapStageData.getData();
        for (final MapStage mapStage : mapStages) {
            System.out.println("Map " + mapStage.getMapStageId() + " " + mapStage.getName());
            for (final MapStageDetail mapDetail : mapStage.getMapStageDetails()) {
                final Deck deck = this.getMapStageDetailDeck(mapStage, mapDetail);
                DeckUtils.writeDeckToFile(deck, false);
                System.out.println(mapDetail.getName());
                final List<Level> levels = mapDetail.getLevels();
                if (levels != null) {
                    for (final Level level : levels) {
                        System.out.println(level);
                    }
                }
            }
        }
    }
    
    private Deck getMapStageDetailDeck(final MapStage mapStage, final MapStageDetail mapDetail) {
        final Deck deck = new Deck();
        return deck;
    }
    
    public static void main(final String[] args) throws IOException {
        final String[] cards = MapUtils.cardList.split(",");
        final List<Card> cardListe = new ArrayList<Card>(cards.length);
        for (int i = 0; i < cards.length; ++i) {
            final String[] cardProps = cards[i].split("_");
            final Card c = new Card();
            c.setCardId(cardProps[0]);
            c.setLevel(cardProps[1]);
            if (cardProps.length == 3) {
                c.setSkillNew(cardProps[2]);
            }
            cardListe.add(c);
        }
        for (final Card c2 : cardListe) {
            System.out.println(c2);
        }
        final EkClient client = new EkClient();
        final String jsonStr = client.getServerCardsAsJson("aeon");
        System.out.println(jsonStr);
        System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse(jsonStr)));
    }
    
    static {
        MapUtils.cardList = "7106_10,7082_10,181_15_688,320_15_688,323_15_688,310_15_688,257_15_1034,312_15_688,250_15_688,260_15_688";
    }
}

package ekutils;

import org.apache.logging.log4j.LogManager;
import java.io.IOException;
import java.util.List;
import com.google.common.io.Files;
import com.google.common.base.Charsets;
import java.io.File;
import servconn.client.EkUserClient;
import org.apache.logging.log4j.Logger;

public class ClaimUtils
{
    static final Logger LOGGER;
    private String argument;
    private EkUserClient client;
    
    public ClaimUtils(final EkUserClient client) {
        this.client = client;
    }
    
    public void setArgument(final String claimGiftCodeArgument) {
        this.argument = claimGiftCodeArgument;
    }
    
    public void claimGiftCode() throws IOException, InterruptedException {
        final File file = new File(this.argument);
        if (file.exists() && !file.isDirectory()) {
            ClaimUtils.LOGGER.info("Reading gift codes from file {}", file.getAbsolutePath());
            final List<String> lines = Files.readLines(file, Charsets.UTF_8);
            for (final String line : lines) {
                this.claimGiftCode(line);
                Thread.sleep(200L);
            }
        }
        else {
            this.claimGiftCode(this.argument);
        }
    }
    
    private void claimGiftCode(final String giftCode) throws IOException {
        try {
            this.client.claimGiftCode(giftCode);
            ClaimUtils.LOGGER.info("Claimed gift code {} with success, check your inbox", giftCode);
        }
        catch (IllegalStateException ise) {
            ClaimUtils.LOGGER.warn("{} failed: {}", giftCode, ise.getMessage());
        }
    }
    
    static {
        LOGGER = LogManager.getLogger();
    }
}

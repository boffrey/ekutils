package ekutils.options;

public class BootException extends RuntimeException
{
	private static final long serialVersionUID = 8348294227440482888L;

	public BootException() {
    }
    
    public BootException(final String arg0) {
        super(arg0);
    }
    
    public BootException(final Throwable arg0) {
        super(arg0);
    }
    
    public BootException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }
    
    public BootException(final String arg0, final Throwable arg1, final boolean arg2, final boolean arg3) {
        super(arg0, arg1, arg2, arg3);
    }
}

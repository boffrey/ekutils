package ekutils.options;

import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
import ekutils.UtilCons;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.lang3.math.NumberUtils;
import java.util.Arrays;
import org.apache.commons.lang3.EnumUtils;
import ekdao.util.Buying;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.DefaultParser;
import java.util.Set;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.Option;

public class BootOptions
{
    public static String HELP_HEADER;
    private static String HELP_USAGE;
    static final Option OPT_HELP;
    static final Option OPT_DEBUG;
    static final Option OPT_DOWNLOAD;
    static final Option OPT_PRINTDECKS;
    static final Option OPT_PRINTCARDS;
    static final Option OPT_FILTERCARDDB;
    static final Option OPT_MAZECLEARCHESTS;
    static final Option OPT_MAZECLEARMONSTERS;
    static final Option OPT_MAZECLEARSTAIRS;
    static final Option OPT_MAZEFINISH;
    static final Option OPT_MAZELEAVEONE;
    static final Option OPT_MAZESTATUS;
    static final Option OPT_BUY;
    static final Option OPT_CLAIMGIFT;
    static final Option OPT_HYDRASTAT;
    static final Option OPT_CLAIMHYDRA;
    private static final Option OPT_EWDECK;
    static final Option OPT_MEDITATE;
    static final Option OPT_STATUS;
    static final Option OPT_PRINTEWBOSSES;
    static final Option OPT_PROPERTYFILE;
    private static final String[] sellOptions;
    static final Option OPT_SELLCARDS;
    static final Option OPT_SELLCARDLIMIT;
    private static final Options OPTIONS;
    public static final Option[] ONE_AND_ONLY_ONE;
    static final Option OPT_OUTPUT_PATH;
    static final Option OPT_FOH_INFO;
    static final Option OPT_PRINT_FRIENDS;
    private CommandLine cmd;
    private static String[] downloadResources;
    private static String[] cardTypes;
    private static Set<String> hydraLevels;
    
    public BootOptions(final String[] args) {
        try {
            this.cmd = new DefaultParser().parse(BootOptions.OPTIONS, args);
        }
        catch (ParseException pe) {
            if (pe instanceof MissingArgumentException) {
                final Option opt = ((MissingArgumentException)pe).getOption();
                String exceptionMessage = "";
                if (opt.getArgName() == null) {
                    exceptionMessage = "-" + opt.getOpt() + " option needs argument, " + opt.getDescription();
                }
                else {
                    exceptionMessage = "-" + opt.getOpt() + " option needs [" + opt.getArgName() + "] argument to work";
                }
                throw new BootException(exceptionMessage);
            }
            throw new BootException(pe.getMessage());
        }
        if (this.cmd.hasOption(BootOptions.OPT_HELP.getOpt())) {
            throw new BootException("");
        }
        boolean foundOne = false;
        for (final Option opt2 : BootOptions.ONE_AND_ONLY_ONE) {
            if (this.cmd.hasOption(opt2.getOpt())) {
                if (foundOne) {
                    throw new BootException("You must specify only one of the following options: " + this.optsToString(BootOptions.ONE_AND_ONLY_ONE));
                }
                foundOne = true;
            }
        }
        if (!foundOne) {
            printHelp(BootOptions.HELP_HEADER);
        }
        else {
            System.out.println(BootOptions.HELP_HEADER);
        }
        this.validateOption(this.cmd, BootOptions.OPT_BUY);
        this.validateOption(this.cmd, BootOptions.OPT_SELLCARDS);
        this.validateOption(this.cmd, BootOptions.OPT_SELLCARDLIMIT);
    }
    
    private void validateOption(final CommandLine cmdLine, final Option opt) {
        if (cmdLine.hasOption(opt.getOpt())) {
            if (BootOptions.OPT_BUY.equals(opt)) {
                final String[] optValues = cmdLine.getOptionValues(BootOptions.OPT_BUY.getOpt());
                final boolean foundGood = EnumUtils.isValidEnum(Buying.class, optValues[0]);
                if (!foundGood) {
                    throw new BootException("You must provide a valid buying good name, valid options: " + Arrays.toString(Buying.values()));
                }
                final boolean amountValid = NumberUtils.isNumber(optValues[1]);
                if (!amountValid) {
                    throw new BootException("You must supply a numeric value for buying amount between [1,100]");
                }
                final Integer amount = Integer.parseInt(optValues[1]);
                if (amount < 1 || amount > 100) {
                    throw new BootException("Amount must be in interval [1,100]");
                }
            }
            else if (BootOptions.OPT_SELLCARDS.equals(opt)) {
                final String sellCardArg = this.getSellCardArgument();
                final boolean validArgument = Arrays.asList(BootOptions.sellOptions).contains(sellCardArg);
                if (!validArgument) {
                    throw new BootException("You must provide a valid selling option, valid options: " + Arrays.toString(BootOptions.sellOptions));
                }
            }
            else if (BootOptions.OPT_SELLCARDLIMIT.equals(opt)) {
                final boolean usedWithSellOption = this.isSellCards();
                if (!usedWithSellOption) {
                    throw new BootException("Limit option can only be used with --sell-cards");
                }
                final Integer limit = this.getSellCardLimit();
                if (limit < 1) {
                    throw new BootException("You must supply a numeric value greater than 1 for selling limit");
                }
            }
        }
    }
    
    private String optsToString(final Option[] oneAndOnlyOne) {
        return null;
    }
    
    public boolean isDebug() {
        return this.cmd.hasOption(BootOptions.OPT_DEBUG.getOpt());
    }
    
    static void checkRequirement(final CommandLine cmd, final Option requiredOption, final Option[] requiredBy) {
        for (final Option opt : requiredBy) {
            if (cmd.hasOption(opt.getOpt()) && !cmd.hasOption(requiredOption.getOpt())) {
                throw new BootException("Option '" + opt.getOpt() + "' requires you to define option '" + requiredOption.getOpt() + "' too.");
            }
        }
    }
    
    public static void printHelp(final String header) {
        final HelpFormatter formatter = new HelpFormatter();
        formatter.setWidth(100);
        formatter.printHelp(BootOptions.HELP_USAGE, header.isEmpty() ? header : (UtilCons.NL + UtilCons.LINE_SEPARATOR + UtilCons.NL + header + UtilCons.NL + UtilCons.LINE_SEPARATOR), BootOptions.OPTIONS, UtilCons.LINE_SEPARATOR);
    }
    
    public boolean isDownloadResources() {
        return this.cmd.hasOption(BootOptions.OPT_DOWNLOAD.getOpt());
    }
    
    public List<String> getDownloadResources() {
        List<String> downloadList = new ArrayList<String>();
        if (this.cmd.hasOption(BootOptions.OPT_DOWNLOAD.getOpt())) {
            final String[] optionValues = this.cmd.getOptionValues(BootOptions.OPT_DOWNLOAD.getOpt());
            if (optionValues == null) {
                downloadList = Arrays.asList(BootOptions.downloadResources);
            }
            else {
                downloadList = Arrays.asList(optionValues);
            }
        }
        return downloadList;
    }
    
    public boolean isMazeClearChests() {
        return this.cmd.hasOption(BootOptions.OPT_MAZECLEARCHESTS.getOpt());
    }
    
    public boolean isMazeClearMonsters() {
        return this.cmd.hasOption(BootOptions.OPT_MAZECLEARMONSTERS.getOpt());
    }
    
    public boolean isMazeClearStairs() {
        return this.cmd.hasOption(BootOptions.OPT_MAZECLEARSTAIRS.getOpt());
    }
    
    public boolean isMazeLeaveOne() {
        return this.cmd.hasOption(BootOptions.OPT_MAZELEAVEONE.getOpt());
    }
    
    public boolean isMazeClearFull() {
        return this.cmd.hasOption(BootOptions.OPT_MAZEFINISH.getOpt());
    }
    
    public boolean isPlayMaze() {
        return this.isMazeClearChests() || this.isMazeClearStairs() || this.isMazeLeaveOne() || this.isMazeClearMonsters() || this.isMazeClearFull();
    }
    
    public int getMazeMapStage() {
        String mapStageText = "-1";
        
        if (this.cmd.hasOption(BootOptions.OPT_MAZECLEARCHESTS.getOpt())) {
            mapStageText = this.cmd.getOptionValue(BootOptions.OPT_MAZECLEARCHESTS.getOpt());
        }
        else if (this.cmd.hasOption(BootOptions.OPT_MAZECLEARSTAIRS.getOpt())) {
            mapStageText = this.cmd.getOptionValue(BootOptions.OPT_MAZECLEARSTAIRS.getOpt());
        }
        else if (this.cmd.hasOption(BootOptions.OPT_MAZELEAVEONE.getOpt())) {
        	mapStageText = this.cmd.getOptionValue(BootOptions.OPT_MAZELEAVEONE.getOpt());
        }
        else if (this.cmd.hasOption(BootOptions.OPT_MAZECLEARMONSTERS.getOpt())) {
            mapStageText = this.cmd.getOptionValue(BootOptions.OPT_MAZECLEARMONSTERS.getOpt());
        }
        else if (this.cmd.hasOption(BootOptions.OPT_MAZEFINISH.getOpt())) {
            mapStageText = this.cmd.getOptionValue(BootOptions.OPT_MAZEFINISH.getOpt());
        }

        if ("kt".equalsIgnoreCase(mapStageText)) {
        	return 99;
        }
        
        int mapStageId = Integer.parseInt(mapStageText);
        if (mapStageId > 10 || mapStageId < 2) {
            mapStageId = -1;
        }
        return mapStageId;
    }
    
    public String getOutputPath() {
    	if(!this.cmd.hasOption(BootOptions.OPT_OUTPUT_PATH.getOpt())) {
    		return "./";
    	}
    	
    	return this.cmd.getOptionValue(BootOptions.OPT_OUTPUT_PATH.getOpt());
    }
    
    public boolean isPrintDecks() {
        return this.cmd.hasOption(BootOptions.OPT_PRINTDECKS.getOpt());
    }
    
    public boolean isPrintDecksForEku() {
        boolean forEku = false;
        if (this.isPrintDecks()) {
            forEku = "eku".equals(this.cmd.getOptionValue(BootOptions.OPT_PRINTDECKS.getOpt()));
        }
        return forEku;
    }
    
    public boolean isPrintCards() {
        return this.cmd.hasOption(BootOptions.OPT_PRINTCARDS.getOpt());
    }
    
    public List<String> getCardTypesForPrinting() {
        List<String> cardTypeList = new ArrayList<String>();
        if (this.cmd.hasOption(BootOptions.OPT_PRINTCARDS.getOpt())) {
            final String[] optionValues = this.cmd.getOptionValues(BootOptions.OPT_PRINTCARDS.getOpt());
            if (optionValues == null) {
                cardTypeList = Arrays.asList(BootOptions.cardTypes);
            }
            else {
                cardTypeList = Arrays.asList(optionValues);
            }
        }
        return cardTypeList;
    }
    
    public boolean isFilterCards() {
        return this.cmd.hasOption(BootOptions.OPT_FILTERCARDDB.getOpt());
    }
    
    public List<String> getCardFilters() {
        List<String> cardFilterList = new ArrayList<String>();
        if (this.cmd.hasOption(BootOptions.OPT_FILTERCARDDB.getOpt())) {
            final String[] optionValues = this.cmd.getOptionValues(BootOptions.OPT_FILTERCARDDB.getOpt());
            if (optionValues != null) {
                cardFilterList = Arrays.asList(optionValues);
            }
        }
        return cardFilterList;
    }
    
    public boolean isBuyGoods() {
        return this.cmd.hasOption(BootOptions.OPT_BUY.getOpt());
    }
    
    public List<String> getBuyingGoodArguments() {
        final String[] optionValues = this.cmd.getOptionValues(BootOptions.OPT_BUY.getOpt());
        final Buying buy = Buying.valueOf(optionValues[0]);
        final List<String> optValueList = new ArrayList<String>();
        optValueList.add(buy.getValue());
        optValueList.add(optionValues[1]);
        return optValueList;
    }
    
    public boolean isClaimGiftCode() {
        return this.cmd.hasOption(BootOptions.OPT_CLAIMGIFT.getOpt());
    }
    
    public String getClaimGiftCodeArgument() {
        String claimArgument = "";
        if (this.isClaimGiftCode()) {
            claimArgument = this.cmd.getOptionValue(BootOptions.OPT_CLAIMGIFT.getOpt());
        }
        return claimArgument;
    }
    
    public boolean isHydraStats() {
        return this.cmd.hasOption(BootOptions.OPT_HYDRASTAT.getOpt());
    }
    
    public boolean isClaimHydra() {
        return this.cmd.hasOption(BootOptions.OPT_CLAIMHYDRA.getOpt());
    }
    
    public List<String> getHydraLevelsForClaiming() {
        final List<String> hydraLevelList = new ArrayList<String>();
        if (this.cmd.hasOption(BootOptions.OPT_CLAIMHYDRA.getOpt())) {
            final String[] optionValues = this.cmd.getOptionValues(BootOptions.OPT_CLAIMHYDRA.getOpt());
            if (optionValues != null) {
                for (int i = 0; i < optionValues.length; ++i) {
                    if (BootOptions.hydraLevels.contains(optionValues[i])) {
                        hydraLevelList.add(optionValues[i]);
                    }
                }
            }
        }
        if (hydraLevelList.isEmpty()) {
            throw new IllegalArgumentException("Wrong argument for claiming, valid options for claiming: h1 h2 h3 h4 h5");
        }
        return hydraLevelList;
    }
    
    public boolean isPrintEWDeck() {
        return this.cmd.hasOption(BootOptions.OPT_EWDECK.getOpt());
    }
    
    public String getPrintEWDeckArgument() {
        String deckString = "";
        if (this.isPrintEWDeck()) {
            deckString = this.cmd.getOptionValue(BootOptions.OPT_EWDECK.getOpt());
        }
        return deckString;
    }
    
    public boolean isMeditate() {
        return this.cmd.hasOption(BootOptions.OPT_MEDITATE.getOpt());
    }
    
    public Integer getMeditateArgument() {
        Integer numberOfMeditations = -1;
        if (this.isMeditate()) {
            try {
                numberOfMeditations = Integer.parseInt(this.cmd.getOptionValue(BootOptions.OPT_MEDITATE.getOpt()));
            }
            catch (NumberFormatException nfe) {
                numberOfMeditations = -1;
            }
        }
        return numberOfMeditations;
    }
    
    public boolean isStatus() {
        return this.cmd.hasOption(BootOptions.OPT_STATUS.getOpt());
    }
    
    public boolean isPrintEwBossDecks() {
        return this.cmd.hasOption(BootOptions.OPT_PRINTEWBOSSES.getOpt());
    }
    
    public boolean isPropertyFile() {
        return this.cmd.hasOption(BootOptions.OPT_PROPERTYFILE.getOpt());
    }
    
    public String getPropertyFile() {
        String propertyFile = "";
        if (this.isPropertyFile()) {
            propertyFile = this.cmd.getOptionValue(BootOptions.OPT_PROPERTYFILE.getOpt());
        }
        return propertyFile;
    }
    
    public boolean isSellCards() {
        return this.cmd.hasOption(BootOptions.OPT_SELLCARDS.getOpt());
    }

	public boolean isFohInfo() {
        return this.cmd.hasOption(BootOptions.OPT_FOH_INFO.getOpt());
	}

	public boolean isPrintFriends() {
        return this.cmd.hasOption(BootOptions.OPT_PRINT_FRIENDS.getOpt());
	}
	
    public boolean isMazeStatus() {
        return this.cmd.hasOption(BootOptions.OPT_MAZESTATUS.getOpt());
    }
    
    public String getSellCardArgument() {
        String arg = "";
        if (this.isSellCards()) {
            arg = this.cmd.getOptionValue(BootOptions.OPT_SELLCARDS.getOpt());
        }
        return arg;
    }
    
    public Integer getSellCardLimit() {
        Integer limit = 0;
        if (this.cmd.hasOption(BootOptions.OPT_SELLCARDLIMIT.getOpt())) {
        	String limitValue = this.cmd.getOptionValue(BootOptions.OPT_SELLCARDLIMIT.getOpt());
        	
        	if (limitValue=="all") {
        		return Integer.MAX_VALUE;
        	}
        	
            try {
                limit = Integer.parseInt(limitValue);
            }
            catch (NumberFormatException e) {
                throw new BootException(e.getMessage());
            }
        }
        return limit;
    }
    
    static {
        BootOptions.HELP_HEADER = "EkUtils - " + UtilCons.VERSION + " (Boffrey's version)" + UtilCons.NL +
        		"https://bitbucket.org/boffrey/ekutils/wiki/" + UtilCons.NL + 
        		"Based upon the original code by Sari" + UtilCons.NL + 
        		"[WARNING] Do not play the game while running ekutils" + UtilCons.NL;
        BootOptions.HELP_USAGE = "ekutils.exe [options] [> output.txt]";
        OPT_HELP = new Option("h", "help", false, "Print this help");
        OPT_DEBUG = Option.builder("d").longOpt("debug").desc("Sets the debug mode which will print alot of information on what's going on.").build();
        OPT_DOWNLOAD = Option.builder("dr").longOpt("download-resource").desc("Download resources from the server, valid options: cards skills runes").hasArgs().optionalArg(true).build();
        OPT_PRINTDECKS = Option.builder("pd").longOpt("print-decks").desc(String.join((CharSequence)"\n", new CharSequence[] { "Print user decks to separate files", "You can use -pd eku to print decks in eku compatible format" })).hasArg(true).optionalArg(true).argName("format").build();
        OPT_PRINTCARDS = Option.builder("pc").longOpt("print-cards").desc("valid options: 4star 5star feast gold special frags").hasArgs().optionalArg(true).argName("card type").build();
        OPT_FILTERCARDDB = Option.builder("fc").longOpt("filter-carddb").desc("valid options: thiefdrops kwpool").hasArgs().build();
        OPT_MAZECLEARCHESTS = Option.builder("mcc").longOpt("maze-clear-chests").desc("Fight only the chests in a given maze,          valid options: 2 3 4 5 6 7 8 9 10 kt").hasArg().argName("map number").build();
        OPT_MAZECLEARMONSTERS = Option.builder("mcm").longOpt("maze-clear-monsters").desc("Fight only the monsters in a given maze,    valid options: 2 3 4 5 6 7 8 9 10 kt").hasArg().argName("map number").build();
        OPT_MAZECLEARSTAIRS = Option.builder("mcs").longOpt("maze-clear-stairs").desc("Clear the stairs in a given maze,               valid options: 2 3 4 5 6 7 8 9 10 kt").hasArg().argName("map number").build();
        OPT_MAZEFINISH = Option.builder("mcf").longOpt("maze-clear-floors").desc("Clear everything in a given maze,                    valid options: 2 3 4 5 6 7 8 9 10 kt").hasArg().argName("map number").build();
        OPT_MAZELEAVEONE = Option.builder("mlo").longOpt("maze-leave-one").desc("Leave only one chest or monster in a given maze,      valid options: 2 3 4 5 6 7 8 9 10 kt").hasArg().argName("map number").build();
        OPT_MAZESTATUS = Option.builder("ms").longOpt("maze-status").desc("Show status for mazes").hasArg(false).build();
        OPT_BUY = Option.builder("buy").longOpt("buy-goods").desc("Buy goods from store like gold packs, booster cards etc" + UtilCons.NL + "valid options: boostercard, boosterpack, goldpack, goldpack10x, firetoken, firetoken10x").hasArgs().argName("[good] [amount]").numberOfArgs(2).build();
        OPT_CLAIMGIFT = Option.builder("cg").longOpt("claim-giftcode").desc("Claim the given gift code or test gift codes from file").hasArg().argName("gift-code> <file-name").build();
        OPT_HYDRASTAT = Option.builder("hs").longOpt("hydra-stats").desc("Print out some useful stats about you and your clan for the ongoing hydra event").hasArg(false).build();
        OPT_CLAIMHYDRA = Option.builder("ch").longOpt("claim-hydra").desc("Claim your hydras, valid options: h1 h2 h3 h4 h5").hasArgs().argName("hydra level").build();
        OPT_EWDECK = Option.builder("pew").longOpt("print-ewdeck").desc("Constructs and prints EW deck from a given deck string").hasArg().argName("deckString").build();
        OPT_MEDITATE = Option.builder("med").longOpt("meditate").desc("Meditate in the temple").hasArg().argName("numOfMeditations").build();
        OPT_STATUS = Option.builder("status").desc("Tell me about myself").hasArg(false).build();
        OPT_PRINTEWBOSSES = Option.builder("wewb").longOpt("write-ew-bosses").desc("Write elemental wars boss decks to separate files").hasArg(false).build();
        OPT_PROPERTYFILE = Option.builder("pf").longOpt("property-file").desc("use a property file").hasArg().build();
        sellOptions = new String[] { "1stars", "2stars", "3stars", "golds", "all" };
        OPT_SELLCARDS = Option.builder("sc").longOpt("sell-cards").desc(String.join((CharSequence)"\n", new CharSequence[] { "sell unenchanted, unlocked 1-2-3 star faction cards or goldies", "Valid options: " + Arrays.toString(BootOptions.sellOptions), "--sell-cards all will sell all 1-2-3 stars and golds, again unenchanted and unlocked", "You may limit the number of cards to be sold with --limit <selling_limit> option ", " where selling_limit is the limit you define" })).hasArg().argName("card type").build();
        OPT_SELLCARDLIMIT = Option.builder("limit").longOpt("limit").desc("limit number of cards to be sold").hasArg().argName("limit amount").build();
        OPT_OUTPUT_PATH = Option.builder("out").longOpt("output-path").desc("specify the output path to write deck files to.").hasArg().argName("output path").build();
        OPT_FOH_INFO = Option.builder("foh").longOpt("foh-info").desc("get Field of Honor info.").hasArg(false).build();
        OPT_PRINT_FRIENDS = Option.builder("friends").longOpt("print-friends").desc("get Friends info.").hasArg(false).build();
        OPTIONS = new Options()
        		.addOption(BootOptions.OPT_DEBUG)
        		.addOption(BootOptions.OPT_HELP)
        		.addOption(BootOptions.OPT_DOWNLOAD)
        		.addOption(BootOptions.OPT_PRINTDECKS)
        		.addOption(BootOptions.OPT_PRINTCARDS)
        		.addOption(BootOptions.OPT_FILTERCARDDB)
        		.addOption(BootOptions.OPT_BUY)
        		.addOption(BootOptions.OPT_CLAIMGIFT)
        		.addOption(BootOptions.OPT_HYDRASTAT)
        		.addOption(BootOptions.OPT_CLAIMHYDRA)
        		.addOption(BootOptions.OPT_EWDECK)
        		.addOption(BootOptions.OPT_MEDITATE)
        		.addOption(BootOptions.OPT_STATUS)
        		.addOption(BootOptions.OPT_PRINTEWBOSSES)
        		.addOption(BootOptions.OPT_PROPERTYFILE)
        		.addOption(BootOptions.OPT_SELLCARDS)
        		.addOption(BootOptions.OPT_SELLCARDLIMIT)
        		.addOption(BootOptions.OPT_MAZEFINISH)
        		.addOption(BootOptions.OPT_MAZELEAVEONE)
        		.addOption(BootOptions.OPT_MAZECLEARCHESTS)
        		.addOption(BootOptions.OPT_MAZESTATUS)
        		.addOption(BootOptions.OPT_OUTPUT_PATH)
        		.addOption(BootOptions.OPT_FOH_INFO)
        		.addOption(BootOptions.OPT_PRINT_FRIENDS);
        ONE_AND_ONLY_ONE = new Option[] { 
        		BootOptions.OPT_DOWNLOAD, 
        		BootOptions.OPT_PRINTDECKS, 
        		BootOptions.OPT_PRINTCARDS, 
        		BootOptions.OPT_FILTERCARDDB, 
        		BootOptions.OPT_MAZECLEARCHESTS, 
        		BootOptions.OPT_MAZECLEARSTAIRS, 
        		BootOptions.OPT_MAZELEAVEONE, 
        		BootOptions.OPT_MAZECLEARMONSTERS, 
        		BootOptions.OPT_MAZESTATUS, 
        		BootOptions.OPT_MAZEFINISH, 
        		BootOptions.OPT_BUY, 
        		BootOptions.OPT_CLAIMGIFT, 
        		BootOptions.OPT_HYDRASTAT, 
        		BootOptions.OPT_SELLCARDS, 
        		BootOptions.OPT_CLAIMHYDRA, 
        		BootOptions.OPT_EWDECK, 
        		BootOptions.OPT_MEDITATE, 
        		BootOptions.OPT_STATUS, 
        		BootOptions.OPT_PRINTEWBOSSES,
        		BootOptions.OPT_FOH_INFO,
        		BootOptions.OPT_PRINT_FRIENDS};
        BootOptions.downloadResources = new String[] { "cards", "skills", "runes" };
        BootOptions.cardTypes = new String[] { "4star", "5star", "feast", "gold", "special", "frags" };
        BootOptions.hydraLevels = new HashSet<String>(Arrays.asList("h1", "h2", "h3", "h4", "h5"));
    }
}

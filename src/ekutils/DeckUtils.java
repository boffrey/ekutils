package ekutils;

import org.apache.logging.log4j.LogManager;
import com.google.common.base.Splitter;
import com.google.common.io.Files;
import com.google.common.base.Charsets;
import java.io.File;
import servconn.dto.userdeck.UserRuneInfo;
import servconn.dto.usercard.UserCard;
import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;
import java.io.IOException;
import servconn.dto.common.Deck;
import java.util.List;
import servconn.dto.userdeck.UserDeckData;
import servconn.dto.user.UserInfo;
import servconn.dto.userdeck.Group;
import ekutils.printer.RunePrinterFactory;
import ekutils.printer.CardPrinterFactory;
import ekdao.dao.DaoFactory;
import ekutils.printer.RunePrinter;
import ekutils.printer.CardPrinter;
import ekdao.dao.RuneDao;
import ekdao.dao.SkillDao;
import ekdao.dao.CardDao;
import servconn.client.EkUserClient;
import org.apache.logging.log4j.Logger;

public class DeckUtils
{
    static final Logger LOGGER;
    private EkUserClient client;
    private CardDao cardDao;
    private SkillDao skillDao;
    private RuneDao runeDao;
    private CardPrinter cardPrinter;
    private RunePrinter runePrinter;
    private boolean outputToConsole;
    private String printerType;
	private static String outputPath = "./";
    private static Integer EW_CardIndex;
    private static Integer EW_LevelIndex;
    private static Integer EW_SkillIndex;
    
    public DeckUtils(final EkUserClient client, final String printerType) {
        this.client = client;
        this.printerType = printerType;
        this.cardDao = DaoFactory.getCardDao(UtilCons.CARDSFILE);
        this.skillDao = DaoFactory.getSkillDao(UtilCons.SKILLSFILE);
        this.runeDao = DaoFactory.getRuneDao(UtilCons.RUNESFILE);
        (this.cardPrinter = CardPrinterFactory.getCardPrinter(printerType)).setCardDao(this.cardDao);
        this.cardPrinter.setSkillDao(this.skillDao);
        (this.runePrinter = RunePrinterFactory.getRunePrinter(printerType)).setRuneDao(this.runeDao);
    }
    
    public void printDecks() throws IOException {
        final UserInfo userInfo = this.client.getUserInfo();
        final UserDeckData userDeckData = this.client.getUserDeckData();
        final List<Group> userDeckList = userDeckData.getGroups();
        boolean rewritePropertyFile = false;
        for (final Group deck : userDeckList) {
            final DeckProperty dp = this.getFilenameForDeckId(deck.getGroupId().toString(), userDeckData);
            rewritePropertyFile = (rewritePropertyFile || dp.isNew);
            final Deck d = this.getCommonDeck(deck, userInfo, dp.deckFileName);
            if ("crys".equals(this.printerType)) {
                writeDeckToFile(d, this.outputToConsole);
            }
            else {
                if (!"eku".equals(this.printerType)) {
                    continue;
                }
                writeDeckToEkuFile(d, this.outputToConsole);
            }
        }
        if (rewritePropertyFile) {
            Config.writePropertiesToFile();
            DeckUtils.LOGGER.info("Added new deck ids to your property file, check and fill them with their corresponding deck names");
        }
    }
    
    private DeckProperty getFilenameForDeckId(final String deckId, final UserDeckData userDeckData) {
        final DeckProperty dp = new DeckProperty();
        String deckName = Config.getProperty(deckId);
        if (StringUtils.isEmpty(deckName)) {
            String prefix = "deck";
            if (deckId.equals(userDeckData.getDefenceGroupid().toString())) {
                prefix = "defense";
            }
            else if (deckId.equals(userDeckData.getLegionWarGroupId().toString())) {
                prefix = "kw";
            }
            deckName = String.format("%s-%s.%s", prefix, deckId, "txt");
            dp.isNew = true;
            Config.addProperty(deckId, "deck".equals(prefix) ? "" : prefix);
        }
        else {
            deckName = String.format("%s-%s.%s", deckName, deckId, "txt");
            dp.isNew = false;
        }
        dp.deckFileName = deckName;
        return dp;
    }
    
    private Deck getCommonDeck(final Group deck, final UserInfo userInfo, final String deckFileName) {
        final Deck d = new Deck();
        d.setPlayerLevel(userInfo.getLevel());
        d.setPlayerName(userInfo.getNickName());
        final List<String> cardList = new ArrayList<String>();
        for (final UserCard card : deck.getUserCardInfo()) {
            cardList.add(this.cardPrinter.printCard(card));
        }
        d.setCardList(cardList);
        final List<String> runeList = new ArrayList<String>();
        final List<UserRuneInfo> userRuneList = deck.getUserRuneInfo();
        for (final UserRuneInfo userRune : userRuneList) {
            runeList.add(this.runePrinter.printRune(userRune));
        }
        d.setRuneList(runeList);
        d.setDeckId(deck.getGroupId().toString());
        d.setDeckFileName(deckFileName);
        return d;
    }
    
    public static void writeDeckToFile(final Deck deck, final boolean toConsole) throws IOException {
        final String deckString = printDeck(deck);
        if (toConsole) {
            System.out.println(deckString);
        }
        else {
        	final File path = new File(outputPath);
            path.mkdirs();
        	
            final File file = new File(outputPath + deck.getDeckFileName());
            DeckUtils.LOGGER.info("writing deck file " + file.getAbsolutePath());
            Files.write(deckString, file, Charsets.UTF_8);
        }
    }
    
    public static void writeDeckToEkuFile(final Deck deck, final boolean toConsole) throws IOException {
        final String deckString = printDeckEku(deck);
        if (toConsole) {
            System.out.println(deckString);
        }
        else {
        	final File path = new File(outputPath);
            path.mkdirs();
        	
            final File file = new File(outputPath + "ekudecks.txt");
            DeckUtils.LOGGER.info("adding deck to file " + file.getAbsolutePath());
            Files.append(deckString, file, Charsets.UTF_8);
        }
    }
    
    public static String printDeck(final Deck deck) {
        final StringBuilder sb = new StringBuilder();
        sb.append("#DeckId " + deck.getDeckId()).append(UtilCons.NL);
        sb.append("#Player info").append(UtilCons.NL);
        sb.append("Player name: ").append(deck.getPlayerName()).append(UtilCons.NL);
        sb.append("Player level: ").append(deck.getPlayerLevel()).append(UtilCons.NL);
        sb.append(UtilCons.NL);
        sb.append("#My cards").append(UtilCons.NL);
        if (deck.getCardList() != null) {
            for (final String cardName : deck.getCardList()) {
            	String actualCardName = fixCardNamesForCrystark(cardName);
                sb.append(actualCardName).append(UtilCons.NL);
            }
        }
        sb.append(UtilCons.NL);
        sb.append("#My runes").append(UtilCons.NL);
        if (deck.getRuneList() != null) {
            for (final String rune : deck.getRuneList()) {
                sb.append(rune).append(UtilCons.NL);
            }
        }
        return sb.toString();
    }
    
    private static String fixCardNamesForCrystark(String cardName) {
		return cardName.replaceFirst("Summon DragonII", "Summon Dragon II");
	}

	public static String printDeckEku(final Deck deck) {
        final StringBuilder sb = new StringBuilder();
        if (deck.getDeckFileName().startsWith("deck")) {
            sb.append("#DeckId " + deck.getDeckId()).append(UtilCons.NL);
        }
        else {
            sb.append("#" + deck.getDeckFileName().substring(0, deck.getDeckFileName().indexOf("."))).append(UtilCons.NL);
        }
        sb.append("#Cards").append(UtilCons.NL);
        if (deck.getCardList() != null) {
            for (final String cardName : deck.getCardList()) {
                sb.append(cardName).append(", ");
            }
            sb.setLength(sb.length() - 2);
        }
        sb.append(UtilCons.NL);
        sb.append("#Runes").append(UtilCons.NL);
        if (deck.getRuneList() != null) {
            for (final String rune : deck.getRuneList()) {
                sb.append(rune).append(", ");
            }
            sb.setLength(sb.length() - 2);
        }
        sb.append(UtilCons.NL).append(UtilCons.NL);
        return sb.toString();
    }
    
    public void printEWDeck(final String deckString) {
        final List<String> cardList = Splitter.on(",").splitToList(deckString);
        System.out.println("#Cards");
        for (final String card : cardList) {
            final List<String> cardLevelSkill = Splitter.on("_").splitToList(card);
            final UserCard userCard = new UserCard();
            try {
                userCard.setCardId(Integer.parseInt(cardLevelSkill.get(DeckUtils.EW_CardIndex)));
                userCard.setLevel(Integer.parseInt(cardLevelSkill.get(DeckUtils.EW_LevelIndex)));
                userCard.setSkillNew(Integer.parseInt(cardLevelSkill.get(DeckUtils.EW_SkillIndex)));
            }
            catch (Exception ex) {}
            System.out.println(this.cardPrinter.printCard(userCard));
        }
    }
    
    public void setConsole(final boolean outputToConsole) {
        this.outputToConsole = outputToConsole;
    }
    
	public void setOutputPath(String outputPath) {
		if(!outputPath.endsWith("/") || !outputPath.endsWith("\\")) {
			outputPath += "/";
		}
		DeckUtils.outputPath = outputPath;
	}
    
    static {
        LOGGER = LogManager.getLogger();
        DeckUtils.EW_CardIndex = 0;
        DeckUtils.EW_LevelIndex = 1;
        DeckUtils.EW_SkillIndex = 2;
    }
    
    private class DeckProperty
    {
        boolean isNew;
        String deckFileName;
    }
}

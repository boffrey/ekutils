package ekutils;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;

import servconn.client.EkUserClient;
import servconn.dto.hydra.FriendPointsList;
import servconn.dto.hydra.HydraPointAward;
import servconn.dto.hydra.HydraRankingData;
import servconn.dto.hydra.LegionHydra;
import servconn.dto.hydra.PersonHydra;
import servconn.dto.hydralist.HydraList;

public class HydraUtils
{
    private static final Integer UNCLAIMED_HYDRA;
    private static final Integer OWN_HYDRA;
    private int[] hydraPointsPerLevel;
    private EkUserClient client;
    private static final String CLANCONTFORMAT = "%-20s %7s";
    
    public HydraUtils(final EkUserClient client) {
        this.hydraPointsPerLevel = new int[] { 200, 600, 1800, 6000, 23000 };
        this.client = client;
    }
    
    public Integer getClanContibutions() throws IOException, InterruptedException {
        final List<FriendPointsList> friendPoints = this.client.getHydraPoints();
        if (friendPoints.size() == 0) {
            System.out.println("---No clan contribution yet---");
            return 0;
        }
        System.out.println("---Clan Contributions---");
        Integer dragonPoints = 0;
        for (final FriendPointsList friendPoint : friendPoints) {
            dragonPoints += Integer.parseInt(friendPoint.getContributePoint());
            System.out.println(String.format(CLANCONTFORMAT, friendPoint.getNickName(), friendPoint.getContributePoint()));
        }
        System.out.println(String.format(CLANCONTFORMAT, "Total contributors:", friendPoints.size()));
        System.out.println(String.format(CLANCONTFORMAT, "Total dragon points:", dragonPoints));
        System.out.println(String.format(CLANCONTFORMAT, "Average:", dragonPoints / friendPoints.size()));
        return dragonPoints;
    }
    
    public Integer printUnclaimedUserHydras() throws IOException {
        final List<HydraList> userHydraList = this.client.getUserHydraList();
        if (userHydraList == null || userHydraList.isEmpty()) {
            System.out.println(UtilCons.NL + "You have no unclaimed Hydras");
            return 0;
        }
        int totalUnclaimedPoints = 0;
        final String hydraPoints = Config.getProperty("hydraPoints");
        if (!StringUtils.isEmpty(hydraPoints)) {
            final String[] points = hydraPoints.split(",");
            for (int i = 0; i < this.hydraPointsPerLevel.length; ++i) {
                this.hydraPointsPerLevel[i] = Integer.parseInt(points[i].trim());
            }
        }
        final Multiset<String> hydraSet = HashMultiset.create();
        for (final HydraList hydra : userHydraList) {
            if (HydraUtils.UNCLAIMED_HYDRA.equals(hydra.getEnableAward())) {
                final int secondSpaceIndex = StringUtils.ordinalIndexOf(hydra.getHydraName(), " ", 2);
                String hydraName = "";
                if (secondSpaceIndex < 0) {
                    hydraName = hydra.getHydraName();
                }
                else {
                    hydraName = hydra.getHydraName().substring(0, secondSpaceIndex);
                }
                hydraSet.add(hydraName);
                totalUnclaimedPoints += this.hydraPointsPerLevel[hydra.getGrade() - 1];
            }
        }
        System.out.println(UtilCons.NL + "---Unclaimed Hydras---");
        for (final String hydraLevel : Multisets.copyHighestCountFirst(hydraSet).elementSet()) {
            final int numberOfHydrasPerLevel = hydraSet.count(hydraLevel);
            System.out.println(String.format("%3d x %s", numberOfHydrasPerLevel, hydraLevel));
        }
        System.out.println("Estimated total unclaimed points: " + totalUnclaimedPoints);
        return totalUnclaimedPoints;
    }
    
    public void claimUserHydras(final List<String> hydraLevels) throws IOException, InterruptedException {
        final List<HydraList> userHydraList = this.client.getUserHydraList();
        final Set<Integer> hydraLevelSet = this.getHydraNamesForLevels(hydraLevels);
        if (hydraLevelSet.isEmpty()) {
            return;
        }
        final Multiset<Integer> hydraAwards = HashMultiset.create();
        final Multiset<Integer> hydraLevelCount = HashMultiset.create();
        final ProgressBar pb = new ProgressBar();
        int index = 0;
        for (final HydraList hydra : userHydraList) {
            if (hydra.getEnableAward().equals(HydraUtils.UNCLAIMED_HYDRA)) {
                //final int secondSpaceIndex = StringUtils.ordinalIndexOf(hydra.getHydraName(), " ", 2);
                if (hydraLevelSet.contains(hydra.getGrade())) {
                    final HydraPointAward award = this.client.claimUserHydra(hydra.getUserHydraId());
                    hydraAwards.add(hydra.getGrade(), award.getTotalPoint());
                    hydraLevelCount.add(hydra.getGrade(), 1);
                    Thread.sleep(200L);
                }
            }
            pb.update(index++, userHydraList.size());
        }
        System.out.println(UtilCons.NL);
        Integer totalClaimedPoints = 0;
        for (final Integer hydraLevel : Multisets.copyHighestCountFirst(hydraAwards).elementSet()) {
            final int hydraPointsbyHydraLevel = hydraAwards.count(hydraLevel);
            totalClaimedPoints += hydraPointsbyHydraLevel;
            System.out.println(String.format("%-9d points from %2d Hydra Level %1d", hydraPointsbyHydraLevel, hydraLevelCount.count(hydraLevel), hydraLevel));
        }
        System.out.println("Total claimed points: " + totalClaimedPoints);
    }
    
    private Set<Integer> getHydraNamesForLevels(final List<String> hydraLevels) {
        final Set<Integer> hydraLevelSet = new HashSet<Integer>();
        for (final String level : hydraLevels) {
            hydraLevelSet.add(Integer.parseInt(level.substring(1)));
        }
        return hydraLevelSet;
    }
    
    public void getHydraRankingSelf(final Integer totalContributions, final Integer unclaimedPoints) throws IOException {
        final HydraRankingData hydraRankingData = this.client.getHydraRankingSelf();
        System.out.println(UtilCons.NL + "---Event Ranking---");
        final List<LegionHydra> legionHydraList = hydraRankingData.getLegionHydra();
        for (final LegionHydra clanRanking : legionHydraList) {
            if (HydraUtils.OWN_HYDRA.equals(clanRanking.getBelong())) {
                System.out.println(String.format("Current clan rank %4d,  %s", clanRanking.getRank(), clanRanking.getDescr1()));
            }
        }
        final List<PersonHydra> personHydraList = hydraRankingData.getPersonHydra();
        for (final PersonHydra personalRanking : personHydraList) {
            if (HydraUtils.OWN_HYDRA.equals(personalRanking.getBelong())) {
                System.out.println(String.format("Your current rank %4d,  %s", personalRanking.getRank(), personalRanking.getDescr1()));
                final Integer selfPoints = Integer.parseInt(personalRanking.getDescr1().substring(personalRanking.getDescr1().indexOf(":") + 1).trim());
                System.out.println(String.format("Estimated total points %4d", selfPoints + totalContributions + unclaimedPoints));
            }
        }
    }
    
    static {
        UNCLAIMED_HYDRA = 1;
        OWN_HYDRA = 1;
    }
}

// 
// EkUtils 
// 

package ekutils;

import java.util.Collections;
import java.util.ArrayList;
import java.io.IOException;
import java.util.List;
import java.util.Iterator;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import ekdao.util.DaoConstants;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import java.io.File;
import com.google.common.collect.HashMultiset;

public class ServerDeckAnalyzer
{
    private String root;
    
    public ServerDeckAnalyzer(final String rootDirectory) {
        this.root = rootDirectory;
    }
    
    public String analyzeDecks() throws IOException {
        final Multiset<String> cardSet = HashMultiset.create();
        final Multiset<String> evoSet = HashMultiset.create();
        final Multiset<String> runeSet = HashMultiset.create();
        int cardCount = 0;
        final File rootDir = new File(this.root);
        final StringBuilder sb = new StringBuilder();
        for (final File deck : Files.fileTreeTraverser().preOrderTraversal(rootDir)) {
            if (deck.isDirectory()) {
                continue;
            }
            final List<String> lines = Files.readLines(deck, Charsets.UTF_8);
            for (final String line : lines) {
                if (!line.isEmpty() && !line.startsWith("#")) {
                    if (line.startsWith("Player")) {
                        continue;
                    }
                    final String[] token = line.split(",");
                    if (token.length == 1) {
                        runeSet.add(token[0]);
                    }
                    else {
                        final String cardName = token[0];
                        cardSet.add(cardName);
                        ++cardCount;
                        if (token.length != 10) {
                            continue;
                        }
                        evoSet.add(token[9]);
                    }
                }
            }
        }
        sb.append("Total number of cards " + cardCount + DaoConstants.NL);
        sb.append("--------CARDS---------" + DaoConstants.NL);
        sb.append("Total Distinct cards " + cardSet.elementSet().size() + DaoConstants.NL);
        sb.append("----------------------" + DaoConstants.NL);
        for (final String card : Multisets.copyHighestCountFirst(cardSet).elementSet()) {
            sb.append(card + " " + cardSet.count(card) + DaoConstants.NL);
        }
        sb.append("--------RUNES---------" + DaoConstants.NL);
        sb.append("Total Distinct runes " + runeSet.elementSet().size() + DaoConstants.NL);
        sb.append("----------------------" + DaoConstants.NL);
        for (final String rune : Multisets.copyHighestCountFirst(runeSet).elementSet()) {
            sb.append(rune + " " + runeSet.count(rune) + DaoConstants.NL);
        }
        sb.append("--------EVOS---------" + DaoConstants.NL);
        sb.append("Total Distinct evos " + evoSet.elementSet().size() + DaoConstants.NL);
        sb.append("----------------------" + DaoConstants.NL);
        for (final String evo : Multisets.copyHighestCountFirst(evoSet).elementSet()) {
            sb.append(evo + " " + evoSet.count(evo) + DaoConstants.NL);
        }
        return sb.toString();
    }
    
    public String analyzePlayers() throws IOException {
        final File rootDir = new File(this.root);
        final List<PlayerInfo> players = new ArrayList<PlayerInfo>();
        for (final File deck : Files.fileTreeTraverser().preOrderTraversal(rootDir)) {
            if (deck.isDirectory()) {
                continue;
            }
            final List<String> lines = Files.readLines(deck, Charsets.UTF_8);
            final PlayerInfo pi = new PlayerInfo();
            for (final String line : lines) {
                if (line.startsWith("Player")) {
                    if (line.startsWith("Player name:")) {
                        pi.playerName = line.substring("Player name:".length()).trim();
                    }
                    else if (line.startsWith("Player level:")) {
                        pi.playerLevel = Integer.parseInt(line.substring("Player level:".length()).trim());
                    }
                }
                if (pi.isValid()) {
                    players.add(pi);
                    break;
                }
            }
        }
        Collections.sort(players, Collections.reverseOrder());
        final StringBuilder sb = new StringBuilder();
        for (final PlayerInfo pi2 : players) {
            sb.append(pi2 + DaoConstants.NL);
        }
        return sb.toString();
    }
    
    public static void main(final String[] args) throws IOException {
        final ServerDeckAnalyzer sda = new ServerDeckAnalyzer("E:\\EkSims\\ek-battlesim\\serenityarena");
        final String players = sda.analyzeDecks();
        System.out.println(players);
    }
    
    private class PlayerInfo implements Comparable<PlayerInfo>
    {
        private String playerName;
        private Integer playerLevel;
        
        @Override
        public int compareTo(final PlayerInfo pi) {
            return Integer.compare(this.playerLevel, pi.playerLevel);
        }
        
        public boolean isValid() {
            return this.playerName != null && this.playerLevel != null;
        }
        
        @Override
        public String toString() {
            return this.playerName + " " + this.playerLevel;
        }
    }
}

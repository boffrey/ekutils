package ekutils.printer;

import org.apache.commons.lang3.StringUtils;

import ekdao.dao.CardDao;
import ekdao.dao.SkillDao;
import servconn.dto.card.Card;
import servconn.dto.skill.Skill;
import servconn.dto.usercard.UserCard;

public class CrystarkCardPrinter implements CardPrinter
{
    private CardDao cardDao;
    private SkillDao skillDao;
    private boolean elemantalWarMode;
    
    public CrystarkCardPrinter() {
        this.elemantalWarMode = false;
    }
    
    @Override
    public String printCard(final UserCard userCard) {
        final Card card = this.cardDao.getCard(userCard.getCardId().toString());
        final StringBuilder sb = new StringBuilder();
        if (this.elemantalWarMode) {
            sb.append("LG:");
        }
        sb.append(card.getCardName()).append(", ").append(userCard.getLevel());
        if (userCard.getSkillNew() != null && !StringUtils.isEmpty(userCard.getSkillNew().toString()) && !"0".equals(userCard.getSkillNew().toString())) {
            final Skill skill = this.skillDao.getSkill(userCard.getSkillNew().toString());
            if (skill != null) {
                sb.append(", ").append(skill.getName());
            }
        }
        return sb.toString();
    }
    
    @Override
    public void setCardDao(final CardDao cardDao) {
        this.cardDao = cardDao;
    }
    
    @Override
    public void setSkillDao(final SkillDao skillDao) {
        this.skillDao = skillDao;
    }
    
    public void setElemantalWarMode(final boolean elemantalWarMode) {
        this.elemantalWarMode = elemantalWarMode;
    }
}

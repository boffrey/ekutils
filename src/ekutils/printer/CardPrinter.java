// 
// EkUtils 
// 

package ekutils.printer;

import ekdao.dao.SkillDao;
import ekdao.dao.CardDao;
import servconn.dto.usercard.UserCard;

public interface CardPrinter
{
    String printCard(final UserCard p0);
    
    void setCardDao(final CardDao p0);
    
    void setSkillDao(final SkillDao p0);
}

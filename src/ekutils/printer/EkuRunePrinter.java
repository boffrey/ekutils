// 
// EkUtils 
// 

package ekutils.printer;

import servconn.dto.rune.Rune;
import servconn.dto.userdeck.UserRuneInfo;
import ekdao.dao.RuneDao;

public class EkuRunePrinter implements RunePrinter
{
    private RuneDao runeDao;
    
    @Override
    public String printRune(final UserRuneInfo userRune) {
        final Rune rune = this.runeDao.getRune(userRune.getRuneId());
        return rune.getRuneName();
    }
    
    @Override
    public void setRuneDao(final RuneDao runeDao) {
        this.runeDao = runeDao;
    }
}

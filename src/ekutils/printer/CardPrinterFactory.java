// 
// EkUtils 
// 

package ekutils.printer;

public class CardPrinterFactory
{
    public static CardPrinter getCardPrinter(final String printerType) {
        if ("crys".equalsIgnoreCase(printerType)) {
            return new CrystarkCardPrinter();
        }
        if ("eku".equalsIgnoreCase(printerType)) {
            return new EkuCardPrinter();
        }
        return null;
    }
}

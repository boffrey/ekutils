// 
// EkUtils 
// 

package ekutils;

import java.io.IOException;
import servconn.dto.usercard.UserCard;
import java.util.List;
import servconn.dto.ew.EwBoss;
import servconn.dto.ew.EwBossCard;
import java.util.ArrayList;
import servconn.dto.common.Deck;
import ekdao.dao.DaoFactory;
import ekdao.dao.SkillDao;
import ekdao.dao.CardDao;
import ekutils.printer.CrystarkCardPrinter;
import servconn.client.EkUserClient;

public class ElementalWarUtils
{
    private EkUserClient client;
    private CrystarkCardPrinter cardPrinter;
    private CardDao cardDao;
    private SkillDao skillDao;
    
    public ElementalWarUtils(final EkUserClient client) {
        this.client = client;
        this.cardDao = DaoFactory.getCardDao(UtilCons.CARDSFILE);
        this.skillDao = DaoFactory.getSkillDao(UtilCons.SKILLSFILE);
        (this.cardPrinter = new CrystarkCardPrinter()).setCardDao(this.cardDao);
        this.cardPrinter.setSkillDao(this.skillDao);
        this.cardPrinter.setElemantalWarMode(true);
    }
    
    public void printElementalWarDecks() throws IOException {
        for (int bossIndex = 1; bossIndex <= 10; ++bossIndex) {
            final EwBoss ewBoss = this.client.getEwBossInfo(bossIndex + "");
            final Deck deck = new Deck();
            deck.setDeckId(bossIndex + "");
            final List<String> cardList = new ArrayList<String>();
            for (int cardIndex = 0; cardIndex < ewBoss.getCards().size(); ++cardIndex) {
                final EwBossCard bossCard = ewBoss.getCards().get(cardIndex);
                final UserCard userCard = this.convertToUserCard(bossCard);
                final String cardLine = this.cardPrinter.printCard(userCard);
                if (cardIndex == 0) {
                    final String cardName = this.cardDao.getCard(bossCard.getCardId().toString()).getCardName();
                    final String legendaryName = cardName.replace(" ", "").replace("(", "-").replace(")", "");
                    deck.setPlayerName(cardName);
                    deck.setDeckFileName(String.format("%02d-%s%s", bossIndex, legendaryName, ".txt"));
                    deck.setPlayerLevel("120");
                }
                cardList.add(cardLine);
            }
            deck.setCardList(cardList);
            DeckUtils.writeDeckToFile(deck, false);
        }
    }
    
    private UserCard convertToUserCard(final EwBossCard bossCard) {
        final UserCard userCard = new UserCard();
        userCard.setCardId(bossCard.getCardId());
        userCard.setLevel(bossCard.getLevel());
        userCard.setEvolution(bossCard.getEvolution());
        userCard.setSkillNew(bossCard.getSkillNew());
        userCard.setWashTime(bossCard.getWashTime());
        return userCard;
    }
}

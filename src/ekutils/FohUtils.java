package ekutils;

import java.io.IOException;
import java.io.StringReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import servconn.client.EkUserClient;
import servconn.dto.foh.TavernAward;
import servconn.dto.foh.TavernData;

public class FohUtils
{
    private EkUserClient client;
    
    public FohUtils(final EkUserClient client) {
        this.client = client;
    }
    
    public void doLottery() throws IOException {
        final TavernData tavernData = this.client.doLottery();
        for (final String result : tavernData.getResult()) {
            System.out.println(result);
        }
        System.out.println(tavernData.getAward());
        if (tavernData.getAward() != null) {
            for (final TavernAward award : tavernData.getAward()) {
                System.out.println(award.getLeaguePoint());
            }
        }
        else {
            System.out.println("You get nothing");
        }
    }
    
    public void doLotteryInfoAsJson() throws IOException {
        final String json = this.client.doLotteryInfoAsJson();
        System.out.println(json);
    }

	public void printFohInfo() throws IOException {
        final String json = this.client.doFohInfoAsJson();
        
        final StringReader sr = new StringReader(json);
        final JsonReader jsonReader = new JsonReader(sr);
        jsonReader.setLenient(true);
        final JsonObject jo = new JsonParser().parse(jsonReader).getAsJsonObject();
        final JsonObject leagueNow =  jo.getAsJsonObject("data").getAsJsonObject("LeagueNow");
        
        System.out.println("FoH League " + leagueNow.get("LeagueId").getAsString());
        System.out.println(leagueNow.getAsJsonObject("Condition").get("Desc").getAsString());
        int roundNumber = leagueNow.get("RoundNow").getAsInt();
        JsonArray roundResult = leagueNow.get("RoundResult").getAsJsonArray();
        for(int i=1; i<= roundNumber; i++) {
            System.out.println();
            System.out.println("Round " + i);
            JsonArray thisResult = roundResult.get(i-1).getAsJsonArray();
            int itemNumber = 0;
            for(JsonElement resultItem : thisResult) {
            	JsonElement player = resultItem.getAsJsonObject();
            	JsonObject playerInfo = player.getAsJsonObject().getAsJsonObject("BattleInfo").getAsJsonObject("User");
            	
            	
        		String name = playerInfo.get("NickName").getAsString();
        		String level = playerInfo.get("Level").getAsString();
                System.out.print(name + ", level " + level);
                if(player.getAsJsonObject().has("BetOdds")) {
                	System.out.print(", odds:" + player.getAsJsonObject().get("BetOdds").getAsString());
                }
                
                if(itemNumber%2==1) {
                	System.out.println();
                }
                else {
                	System.out.print(" vs. ");
                }
                
                itemNumber++;
            }
        }

        System.out.println();
        final JsonObject leagueNext =  jo.getAsJsonObject("data").getAsJsonObject("LeagueNext");
        System.out.println("Next League (" + leagueNext.get("LeagueId").getAsString() + ")");
        System.out.println(leagueNext.getAsJsonObject("Condition").get("Desc").getAsString());
	}
}

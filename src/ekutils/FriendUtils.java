package ekutils;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import servconn.client.EkUserClient;
import servconn.dto.friend.Friend;

public class FriendUtils {

    private EkUserClient client;

	public FriendUtils(final EkUserClient client) {
        this.client = client;
    }
    
	public void GetFriendsList() throws IOException {
		List<Friend> friends = this.client.getFriends();

		Comparator<Friend> comp = (Friend a, Friend b) -> {
		    return a.getLastLogin().compareTo(b.getLastLogin());
		};
		friends.sort(comp);

		addSpacesTo("#", 4);
		addSpacesTo("Nickname", 20);
		addSpacesTo("Level", 6);
		addSpacesTo("Last Login", 12);
		System.out.println();
		Integer count = 1;
		for(Friend friend : friends) {
			addSpacesTo(count.toString(), 4);
			addSpacesTo(friend.getNickName(), 20);
			addSpacesTo(friend.getLevel().toString(), 6);
			addSpacesTo(friend.getLastLogin().toString(), 12);
			System.out.println();
			
			count++;
		}
	}
	
	private void addSpacesTo(String text, int length) {
		System.out.print(text);
		for(int i = text.length(); i<length; i++) {
			System.out.print(" ");
		}
	}
}

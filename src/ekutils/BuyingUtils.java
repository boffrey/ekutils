package ekutils;

import org.apache.logging.log4j.LogManager;
import java.io.IOException;
import java.util.Map;
import servconn.dto.usercard.UserCard;
import servconn.dto.user.UserInfo;
import java.util.List;
import java.util.HashMap;
import java.util.Collections;
import servconn.dto.buy.BuyingResult;
import java.util.ArrayList;
import org.apache.logging.log4j.Logger;
import servconn.client.EkUserClient;

public class BuyingUtils
{
    private EkUserClient client;
    static final Logger LOGGER;
    
    public BuyingUtils(final EkUserClient client) {
        this.client = client;
    }
    
    public void buyGood(final String goodsId, final String amount) throws IOException, InterruptedException {
        final Integer buyingAmount = Integer.parseInt(amount);
        final List<String> cardIds = new ArrayList<String>();
        BuyingResult buyingResult = new BuyingResult();
        final ProgressBar pb = new ProgressBar();
        try {
            for (int i = 0; i < buyingAmount; ++i) {
                buyingResult = this.client.buyGoods(goodsId);
                Thread.sleep(200L);
                cardIds.addAll(buyingResult.getCardIdsAsList());
                pb.update(i, buyingAmount);
            }
        }
        catch (IllegalStateException ie) {
            System.out.println();
            BuyingUtils.LOGGER.warn(ie.getMessage());
        }
        final String cardDrops = EkUtils.printCardDrops(cardIds, Collections.emptyList());
        System.out.println(cardDrops);
        System.out.println(String.format("You have %s golds, %s gems, %s fire tokens currently", buyingResult.getCoinsAfter(), buyingResult.getCashAfter(), buyingResult.getTicketAfter()));
        System.out.println(UtilCons.NL + "Your card stats after purchase");
        final UserInfo userInfo = this.client.getUserInfo();
        final List<UserCard> userCardList = this.client.getUserCards();
        final List<String> lockedUserCardIds = userInfo.getLockedUserCardIds();
        final UserCardDb ucd = new UserCardDb(userCardList, lockedUserCardIds);
        final Map<Integer, Integer> cardStats = new HashMap<Integer, Integer>();
        for (int j = 1; j <= 5; ++j) {
            cardStats.put(j, ucd.getNumberOfFactionCardsByStar(j + ""));
        }
        Integer total = 0;
        for (int k = 1; k <= 5; ++k) {
            total += cardStats.get(k);
            System.out.println("Number of " + k + " star cards: " + cardStats.get(k));
        }
        System.out.println("Total number of cards: " + total);
    }
    
    static {
        LOGGER = LogManager.getLogger();
    }
}

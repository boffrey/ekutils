// 
// EkUtils 
// 

package ekutils;

import com.google.gson.GsonBuilder;
import java.io.IOException;
import servconn.dto.usercard.UserCard;
import java.util.List;
import servconn.dto.user.UserInfo;
import com.google.gson.Gson;
import servconn.client.EkUserClient;

public class UserFunctions
{
    private EkUserClient client;
    protected static Gson GSON;
    Integer LEVEL120;
    Integer MAXLEVEL;
    
    public UserFunctions(final EkUserClient client) {
        this.LEVEL120 = 549887700;
        this.MAXLEVEL = 120;
        this.client = client;
    }
    
    public void userStatus() throws IOException {
        final UserInfo userInfo = this.client.getUserInfo();
        final List<UserCard> userCardList = this.client.getUserCards();
        final List<String> lockedUserCardIds = userInfo.getLockedUserCardIds();
        final UserCardDb ucd = new UserCardDb(userCardList, lockedUserCardIds);
        final StringBuilder sb = new StringBuilder();
        final int playerLevel = Integer.parseInt(userInfo.getLevel());
        sb.append(String.format("Nickname: %s %s", userInfo.getNickName(), UtilCons.NL));
        sb.append(String.format("Member Since: %s %s", userInfo.getRegisterTime(), UtilCons.NL));
        sb.append(String.format("Level %d with deck cost %s %s", playerLevel, userInfo.getLeaderShip(), UtilCons.NL));
        sb.append(String.format("Current Exp: %d %s", userInfo.getExp(), UtilCons.NL));
        if (playerLevel < this.MAXLEVEL) {
            sb.append(String.format("Remaining Exp For Level %d: %d %s", playerLevel + 1, Integer.parseInt(userInfo.getNextExp()) - userInfo.getExp(), UtilCons.NL));
        }
        if (playerLevel < this.MAXLEVEL - 1) {
            sb.append(String.format("Remaining Exp For Level 120 %d: %s", this.LEVEL120 - userInfo.getExp(), UtilCons.NL));
        }
        sb.append(UtilCons.NL);
        sb.append(String.format("Gold: %d Gems: %d FireTokens: %s %s", userInfo.getCoins(), userInfo.getCash(), userInfo.getTicket(), UtilCons.NL));
        sb.append(String.format("Gold shards %s,  Purple shards %s, Blue shards %s %s", userInfo.getFragment5(), userInfo.getFragment4(), userInfo.getFragment3(), UtilCons.NL));
        sb.append(String.format("Current energy %d %s", userInfo.getEnergy(), UtilCons.NL));
        sb.append(UtilCons.NL);
        if (userInfo.getNewEmail() > 0) {
            sb.append(String.format("You have %d email!!! %s", userInfo.getNewEmail(), UtilCons.NL));
        }
        if (userInfo.getBoss() == 1) {
            sb.append(String.format("There is an active demon %s", UtilCons.NL));
        }
        sb.append(String.format("Arena battles left for today: %d %s", userInfo.getRankTimes(), UtilCons.NL));
        sb.append(String.format("Evolution left for today: %d %s", userInfo.getEvolutionTimes(), UtilCons.NL));
        sb.append(UtilCons.NL);
        sb.append(String.format("Number of 1 star cards: %d %s", ucd.getNumberOfFactionCardsByStar("1"), UtilCons.NL));
        sb.append(String.format("Number of 2 star cards: %d %s", ucd.getNumberOfFactionCardsByStar("2"), UtilCons.NL));
        sb.append(String.format("Number of 3 star cards: %d %s", ucd.getNumberOfFactionCardsByStar("3"), UtilCons.NL));
        System.out.println(sb.toString());
    }
    
    static {
        UserFunctions.GSON = new GsonBuilder().setPrettyPrinting().create();
    }
}
